{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           Test.Hspec          (hspec)

import qualified Rx.ObservableTest

main :: IO ()
main = hspec Rx.ObservableTest.tests
