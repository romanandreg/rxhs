{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.ObservableTest where

import Control.Applicative
import Control.Monad (void)
import Control.Exception (SomeException(..))
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar (modifyTVar, newTVarIO, readTVar)
import qualified Control.Concurrent.MVar as MVar
import qualified Control.Concurrent.Chan as Chan
import qualified Control.Concurrent.Async as Async

import qualified Rx.Scheduler as Scheduler
import qualified Rx.Observable as Observable
import qualified Rx.Subject.PublishSubject as Subject
import Rx.Test.Assertions
import Rx.Types


import qualified Rx.Observable.AmbTest
import qualified Rx.Observable.BufferTest
import qualified Rx.Observable.ConcatTest
import qualified Rx.Observable.DistinctTest
import qualified Rx.Observable.ErrorTest
import qualified Rx.Observable.MergeTest
import qualified Rx.Observable.RetryTest
import qualified Rx.Observable.ReplayTest
import qualified Rx.Observable.SwitchTest
import qualified Rx.Observable.TakeTest
import qualified Rx.Observable.TakeUntilTest
import qualified Rx.Observable.ZipTest

import Test.HUnit
import Test.Hspec

tests :: Spec
tests = do
  Rx.Observable.AmbTest.tests
  Rx.Observable.BufferTest.tests
  Rx.Observable.ConcatTest.tests
  Rx.Observable.DistinctTest.tests
  Rx.Observable.ErrorTest.tests
  Rx.Observable.MergeTest.tests
  Rx.Observable.RetryTest.tests
  Rx.Observable.ReplayTest.tests
  Rx.Observable.SwitchTest.tests
  Rx.Observable.TakeTest.tests
  Rx.Observable.TakeUntilTest.tests
  Rx.Observable.ZipTest.tests

  describe "publish" $ do
    it "stops emition of events until a connect call is done" $ do
      countVar <- newTVarIO 0
      subj <- Subject.create
      observable <- Observable.publish (toObservable subj)
      let observer = Observer
                       $ \case
                         OnNext _ -> atomically $ modifyTVar countVar (+1)
                         OnError _ -> return ()
                         OnCompleted -> return ()
      sub1 <- subscribe observable observer
      onNext subj 1
      sub2 <- subscribe observable observer
      onNext subj 2
      callCount0 <- atomically $ readTVar countVar
      assertEqual "should not call the observer until connect" callCount0 0
      Observable.connect observable
      callCount1 <- atomically $ readTVar countVar
      assertEqual "should send pending notifications after connect" callCount1 4
      mapM_ unsubscribe [sub1, sub2]


  describe "flatMap" $ do
    it "manages observable composition successfuly" $ do
      let ob1 = Observable.range 1 10
          ob2 = Observable.flatMap ob1 (Observable.just . (+10))

      assertObservable "observables did not compose correctly"
                       [11..20]
                       ob2


  describe "first" $ do
    it "emits just the first message from observable" $ do
      assertObservable
        "should emit a single element"
        [1]
        (Observable.first $ Observable.fromList [1..10])

  describe "once" $ do
    it "emits an error when more than one message is sent" $ do
      assertObservableFailure
        "should throw an error"
        [1]
        (Observable.once $ Observable.fromList [1, 2])
    it "emits just a single message" $ do
      assertObservable
        "should receive only one message"
        [1]
        (Observable.once $ Observable.just 1)

  describe "scan" $ do
    it "emits state reductions per event" $ do
      let obs1 = Observable.scan (+) 0
                $ Observable.range 1 5

      assertObservable "state emitions are different from expected"
                       [1, 3, 6, 10, 15]
                       obs1
