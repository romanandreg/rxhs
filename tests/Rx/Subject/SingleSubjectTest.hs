module Rx.Subject.SingleSubjectTest where

import qualified Control.Concurrent.STM.TVar as TVar
import Test.HUnit (assertBool, assertEqual)
import Test.Hspec

import qualified Rx.Subject.SingleSubject as Subject
import Rx.Types

singleSubjectCommonSpecs subjectCtor =
  it "has one observer at a time" $ do
    cv0 <- TVar.newTVarIO (0 :: Int)
    cv1 <- TVar.newTVarIO (0 :: Int)
    subj <- subjectCtor
    let observer countVar =
          Observer
            $ \case
                OnNext v -> atomically $ TVar.modifyTVar countVar (+v)
                OnError err -> return ()
                OnCompleted -> return ()

    sub0 <- subscribe subj (observer cv0)
    onNext subj 1
    sub1 <- subscribe subj (observer cv1)
    onNext subj 2
    v0 <- atomically $ TVar.readTVar cv0
    assertEqual "should have value of first onNext call" 1 v0
    v1 <- atomically $ TVar.readTVar cv1
    assertEqual "should have value of second onNext call" 2 v1

tests :: Spec
tests = do
  describe "SingleSubject" $ do
    describe "with queue" $ do
      singleSubjectCommonSpecs Subject.createWithQueue
      it "queue up notifications" $ do
        assertionVar <- TVar.newTVarIO False
        s0 <- Subject.createWithQueue
        onNext s0 True
        sub <- subscribe s0
                $ Observer
                $ \case
                    OnNext v ->
                      liftIO . atomically
                        $ TVar.writeTVar assertionVar v
                    OnError err -> return ()
                    OnCompleted -> return ()
        assertion <- liftIO . atomically $ TVar.readTVar assertionVar
        assertBool "should receive notification" assertion

    describe "without queue" $ do
      singleSubjectCommonSpecs Subject.create
      it "doesn't queue up notifications" $ do
        assertionVar <- TVar.newTVarIO True
        s0 <- Subject.create
        onNext s0 False
        sub <- subscribe s0
                $ Observer
                $ \case
                    OnNext v ->
                      liftIO . atomically
                        $ TVar.writeTVar assertionVar v
                    OnError err -> return ()
                    OnCompleted -> return ()
        assertion <- liftIO . atomically $ TVar.readTVar assertionVar
        assertBool "should not receive notification" assertion
