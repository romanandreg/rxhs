{-# LANGUAGE ScopedTypeVariables #-}
module Rx.SubscriptionTest (tests) where

import qualified Control.Exception as Exception

import Test.Hspec
import Test.HUnit

import Rx.Subscription.Class
import qualified Rx.Subscription.Subscription as Basic
import qualified Rx.Subscription.BooleanSubscription as Boolean
import qualified Rx.Subscription.SingleAssignmentSubscription as SingleAssignment

tests :: Spec
tests = describe "Subscription" $ do
  basic
  boolean
  singleAssignment

basicSubscriptionProperties :: ISubscription a => IO a -> Spec
basicSubscriptionProperties subCtor = do
  it "isUnsubscribe returns false" $ do
    sub <- subCtor
    result <- isUnsubscribed sub
    assertBool "should return false" (not result)

  it "after unsubscribe, isUnsubscribed returns true" $ do
    sub <- subCtor
    unsubscribe sub
    result <- isUnsubscribed sub
    assertBool "should return true" result

basic :: Spec
basic = describe "basic" $ do
  basicSubscriptionProperties Basic.empty

boolean :: Spec
boolean = describe "boolean" $ do
  basicSubscriptionProperties Boolean.create

singleAssignment :: Spec
singleAssignment = describe "singleAssignment" $ do
  describe "set when empty" $ do
    it "assigns subscription to singleAssignment" $ do
      sub <- Basic.empty
      sas <- SingleAssignment.empty
      Exception.catch
        (do SingleAssignment.set sub sas
            assertBool "should not throw exception" True)
        (\(_ :: Exception.SomeException) ->
          assertFailure "should not throw exception")

  describe "set when full" $ do
    it "throws an error" $ do
      sub <- Basic.empty
      sas <- SingleAssignment.create sub
      Exception.catch
        (SingleAssignment.set sub sas >> assertFailure "should throw exception")
        (\(_ :: Exception.SomeException) -> assertBool "" True)

  describe "unsubscribe" $ do
    it "calls inner subscription unsubscribe" $ do
      result <- MVar.newEmptyMVar
      sub <- Basic.create (MVar.putMVar result True)
