module Rx.Observable.DistinctTest where

import Control.Concurrent (threadDelay)
import qualified Rx.Observable as Observable
import qualified Rx.Subject.PublishSubject as Subject
import Rx.Types (toObservable, IObserver(..))
import Rx.Test.Assertions
import Test.Hspec


tests :: Spec
tests = do
  describe "distinct" $ do
    it "emits different values only once" $
      assertAsyncObservable
        "should emit diff values no more than once"
        [1..10]
        (do subj <- Subject.create
            let ob = Observable.distinct $ toObservable subj
            return (subj, ob))
        (\subj -> do
            mapM_ (\i -> onNext subj i) [1..10]
            mapM_ (\i -> onNext subj i) [3, 2, 1]
            onCompleted subj)

  describe "distinctUntilChanged" $ do
    it "emits events only when value changes" $ do
      let obs1 = Observable.distinctUntilChanged
                $ Observable.fromList
                $ (take 10 $ repeat 1) ++
                  (take 5  $ repeat 5) ++
                  (take 3  $ repeat 1)

      assertObservable "expecting elements to not be repeated"
                       [1, 5, 1]
                       obs1
