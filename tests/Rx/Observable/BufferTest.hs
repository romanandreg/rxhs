module Rx.Observable.BufferTest where

import qualified Rx.Observable as Observable
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests = do
  describe "bufferCount" $ do
    it "emits events in buffers of specified count" $ do
      let obs = Observable.bufferCount 5
              $ Observable.range 1 10
      assertObservable "expecting to receive valid buffered values"
                       [[1..5], [6..10]]
                       obs

    it "emits pending events onCompleted" $ do
      let obs = Observable.bufferCount 2
              $ Observable.fromList [1, 2, 3]
      assertObservable "expecting to receive valid buffer values"
                       [[1,2], [3]]
                       obs
    it "emits pending events onError" $ do
      let obs = Observable.bufferCount 2
              $ Observable.fromList [1, 2, 3, error "4", 5]
      assertObservableFailure "expecting to receive valid buffer values"
                              [[1,2], [3]]
                              obs

    it "doesn't emit empty lists on onCompleted" $ do
      let obs = Observable.bufferCount 2
              $ Observable.fromList [1, 2, 3, 4]
      assertObservable "expecting to receive valid buffer values"
                       [[1,2], [3, 4]]
                       obs

    it "doesn't emit empty lists on onError" $ do
      let obs = Observable.bufferCount 2
              $ Observable.fromList [1, 2, 3, 4, error "5", 5]
      assertObservableFailure "expecting to receive valid buffer values"
                              [[1,2], [3, 4]]
                              obs

  describe "bufferConditions" $ do
    it "emits events in buffers of specified count" $ do
      let obs = Observable.bufferConditions (return . (== 2))
                                            (return . (== 8))
              $ Observable.fromList [1, 2, 1, 8, 3, 4, 2, 3, 8]
      assertObservable "expecting to receive valid buffered values"
                       [[2, 1, 8], [2, 3, 8]]
                       obs

    it "emits pending events onCompleted" $ do
      let obs = Observable.bufferConditions (return . (== 2))
                                            (return . (== 8))
              $ Observable.fromList [1, 2, 3]
      assertObservable "expecting to receive valid buffer values"
                       [[2, 3]]
                       obs

    it "emits pending events onError" $ do
      let obs = Observable.bufferConditions (return . (== 2))
                                            (return . (== 8))
              $ Observable.fromList [1, 2, 3, error "4"]
      assertObservableFailure "expecting to receive valid buffer values"
                              [[2, 3]]
                              obs

    it "doesn't emit empty lists on onCompleted" $ do
      let obs = Observable.bufferConditions (return . (== 2))
                                            (return . (== 8))
              $ Observable.fromList [2, 2, 3, 8]
      assertObservable "expecting to receive valid buffer values"
                       [[2,2,3,8]]
                       obs

    it "doesn't emit empty lists on onError" $ do
      let obs = Observable.bufferConditions (return . (== 2))
                                            (return . (== 8))
              $ Observable.fromList [2, 3, 4, error "5", 8]
      assertObservableFailure "expecting to receive valid buffer values"
                              [[2, 3, 4]]
                              obs
