module Rx.Observable.MergeTest (tests) where

import Control.Monad (mapM_, sequence_)
import qualified Rx.Subject.PublishSubject as Subject
import qualified Rx.Observable as Observable
import Rx.Types (toObservable, IObserver(..))
import Rx.Test.Assertions
import Test.Hspec


tests :: Spec
tests = describe "merge" $ do
    it "merges two or more observables" $ do
      assertAsyncObservable "should emit events from all merge sources"
                            [1, 2, 3]
                            (do s1 <- Subject.create
                                s2 <- Subject.create
                                s3 <- Subject.create
                                let ss = [s1, s2, s3]
                                    ob = Observable.mergeList $ map toObservable ss
                                return (ss, ob))
                            (\ss -> do
                                sequence_ $ zipWith onNext ss [1..]
                                mapM_ onCompleted ss)

    it "completes on the first observable that is completed" $ do
      assertAsyncObservable "should not emit events when onCompleted is called"
                            [1, 2, 3]
                            (do s1 <- Subject.create
                                s2 <- Subject.create
                                let obs = Observable.mergeList [ toObservable s1
                                                               , toObservable s2]
                                return ((s1, s2), obs))
                            (\(s1, s2) -> do
                              onNext s1 1
                              onNext s2 2
                              onCompleted s1
                              onNext s2 3
                              onNext s1 4
                              onCompleted s2)


    it "fails on the first observable that fails" $ do
      assertAsyncObservableFailure "should not process onNext after error"
                                   [1, 2]
                                   (do
                                     s1 <- Subject.create
                                     s2 <- Subject.create
                                     let ob = Observable.mergeList [ toObservable s1
                                                                   , toObservable s2]
                                     return ((s1, s2), ob))
                                   (\(s1, s2) -> do
                                     onNext s1 1
                                     onNext s2 2
                                     onNext s1 (error "fail"))
