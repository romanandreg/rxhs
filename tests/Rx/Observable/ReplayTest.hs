{-# LANGUAGE LambdaCase #-}
module Rx.Observable.ReplayTest where

import Control.Concurrent (threadDelay)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar (newTVarIO, readTVar, modifyTVar)
import qualified Control.Concurrent.Async as Async
import qualified Rx.Observable as Observable
import qualified Rx.Subject.PublishSubject as Subject
import Rx.Types (Observable, Notification(..), Observer(..), IObserver(..))

import Test.HUnit (assertEqual)
import Test.Hspec

tests :: Spec
tests = do
  describe "replay" $ do
    it "replays all elements that has been passed" $ do
      subject <- Subject.create
      let obs0 = Observable.toObservable subject
      obs1 <- Observable.replay obs0
      Observable.connect obs1

      acc0 <- newTVarIO []
      acc1 <- newTVarIO []

      s0 <- Async.async
              $ Observable.subscribe obs1
              $ Observer
              $ \case
                  OnNext v -> atomically $ modifyTVar acc0 (v:)
                  OnError _ -> return ()
                  OnCompleted -> return ()

      threadDelay 100

      result0 <- atomically $ readTVar acc0
      assertEqual "should be empty" [] result0

      onNext subject 1
      onNext subject 2
      onNext subject 3

      s1 <- Async.async
              $ Observable.subscribe obs1
              $ Observer
              $ \case
                  OnNext v -> atomically $ modifyTVar acc1 (v:)
                  OnError _ -> return ()
                  OnCompleted -> return ()

      threadDelay 100

      result1 <- atomically $ readTVar acc1
      assertEqual "should not be empty" [3, 2, 1] result1

      onCompleted subject
      mapM_ Async.cancel [s0, s1]


  describe "replayBuffer" $ do
    it "replays elements up until a count" $ do
      subject <- Subject.create
      let obs0 = Observable.toObservable subject
      obs1 <- Observable.replayBuffer 2 obs0
      Observable.connect obs1

      acc0 <- newTVarIO []
      acc1 <- newTVarIO []

      s0 <- Async.async
              $ Observable.subscribe obs1
              $ Observer
              $ \case
                  OnNext v -> atomically $ modifyTVar acc0 (v:)
                  OnError _ -> return ()
                  OnCompleted -> return ()
      threadDelay 100

      result0 <- atomically $ readTVar acc0
      assertEqual "should be empty" [] result0

      onNext subject 1
      onNext subject 2
      onNext subject 3
      onCompleted subject

      s1 <- Async.async
              $ Observable.subscribe obs1
              $ Observer
              $ \case
                  OnNext v -> atomically $ modifyTVar acc1 (v:)
                  OnError _ -> return ()
                  OnCompleted -> return ()
      threadDelay 100

      result1 <- atomically $ readTVar acc1
      assertEqual "should not be empty" [2, 1] result1

      mapM_ Async.cancel [s0, s1]
