module Rx.Observable.TakeUntilTest where

import qualified Rx.Observable as Observable
import qualified Rx.Subject.PublishSubject as Subject
import Rx.Types (toObservable, IObserver(..))
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests =
  describe "takeUntil" $ do
    it "stops source observable when other observable emits a value" $ do
      assertAsyncObservable
        "should emit values until other observable emits values"
        [1, 2]
        (do source <- Subject.create
            other  <- Subject.create
            let ob = Observable.takeUntil (toObservable source)
                                          (toObservable other)
            return ((source, other), ob))
        (\(source, other) -> do
            onNext source 1
            onNext source 2
            onNext other "stop"
            onCompleted other)
