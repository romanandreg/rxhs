module Rx.Observable.ZipTest where

import qualified Rx.Subject.PublishSubject as Subject
import qualified Rx.Observable as Observable
import Rx.Types (toObservable, IObserver(..))
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests =
  describe "zipWith" $ do
    describe "with hot observable" $ do
      it "zips until first observable is done" $ do
        assertAsyncObservable "expecting to receive only 2 entries"
                              [(1, 2, 3), (4, 5, 6)]
                              (do s1 <- Subject.create
                                  s2 <- Subject.create
                                  s3 <- Subject.create
                                  let obs = Observable.zipWith3 (,,)
                                                                (toObservable s1)
                                                                (toObservable s2)
                                                                (toObservable s3)
                                  return ((s1, s2, s3), obs))
                              (\(s1, s2, s3) -> do
                                  onNext s1 1
                                  onNext s3 3
                                  onNext s2 2
                                  onNext s3 6
                                  onNext s2 5
                                  onCompleted s2
                                  onNext s1 4
                                  onNext s1 7
                                  onNext s3 9
                                  onCompleted s3
                                  onCompleted s1)


    describe "with cold observable" $ do
      it "zips until first observable is done" $ do
        let obs = Observable.zipWith3 (,,) (Observable.fromList [1, 4, 7])
                                           (Observable.fromList [2, 5])
                                           (Observable.fromList [3, 6, 9])
        assertObservable "expecting to receive only 2 entries"
                          [(1, 2, 3), (4, 5, 6)]
                          obs

      it "returns new observable with values applied to zipfn" $ do
        let obs = Observable.zipWith (+) (Observable.fromList [1, 3, 5])
                                         (Observable.fromList [2, 4, 6])
        assertObservable "expecting to receive combination of 2 observables"
                          [3, 7, 11]
                          obs
