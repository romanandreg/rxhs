module Rx.Observable.RetryTest where

import qualified Rx.Subject.PublishSubject as Subject
import qualified Rx.Observable as Observable
import Rx.Types (toObservable, IObserver(..))
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests = do
  describe "retryCount" $ do
    it "emits an onError handler when retry count is exhausted" $ do
      assertAsyncObservableFailure "expecting to not receive an onError"
                                   [1, 2, 3]
                                   (do
                                      s1 <- Subject.create
                                      return (s1, Observable.retryCount 2
                                                  $ toObservable s1))
                                   (\s1 -> do
                                       onNext s1 1
                                       onNext s1 (error "1.5")
                                       onNext s1 2
                                       onNext s1 (error "2.5")
                                       onNext s1 3
                                       onNext s1 (error "3.5")
                                       onNext s1 4)

  describe "retry" $ do
    it "never emits an onError handler" $ do
      assertAsyncObservable "expecting to not receive an onError"
                            [1, 2, 3, 4, 5]
                            (do
                                s1 <- Subject.create
                                return (s1, Observable.retry
                                            $ toObservable s1))
                            (\s1 -> do
                                onNext s1 1
                                onNext s1 (error "1.5")
                                onNext s1 2
                                onNext s1 (error "2.5")
                                onNext s1 3
                                onNext s1 (error "3.5")
                                onNext s1 4
                                onNext s1 5
                                onCompleted s1)
