module Rx.Observable.TakeTest where

import Control.Applicative
import Control.Monad (forM_, void)
import Control.Exception (SomeException(..))
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar (modifyTVar, newTVarIO, readTVar)
import qualified Control.Concurrent.MVar as MVar
import qualified Control.Concurrent.Chan as Chan
import qualified Control.Concurrent.Async as Async

import qualified Rx.Scheduler as Scheduler
import qualified Rx.Observable as Observable
import qualified Rx.Subject.PublishSubject as Subject
import Rx.Test.Assertions
import Rx.Types

import Test.HUnit
import Test.Hspec

tests :: Spec
tests = do
  describe "take" $ do
    it "emits onComplete if take receives param with value <= 0" $ do
      forM_ [-3..0] $ \n -> do
        assertObservable ("should return an empty list with argument: " ++ show n)
                         []
                         (Observable.take n $ Observable.fromList [1, 2, 3])
    it "emits onComplete after taking all elements" $ do
      assertObservable "should return an empty list"
                       [1, 2]
                       (Observable.take 2 $ Observable.fromList [1, 2, 3])
