module Rx.Observable.AmbTest (tests) where

import Control.Monad (mapM_)
import qualified Rx.Subject.PublishSubject as Subject
import qualified Rx.Observable as Observable
import Rx.Types (IObserver(..))
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests = describe "amb" $ do
  it "emits elements from the first observable that emits" $ do
    assertAsyncObservable "should emit events from fist observable"
                          [0..10]
                          (do s1 <- Subject.create
                              s2 <- Subject.create
                              s3 <- Subject.create
                              let ob = Observable.amb [s1, s2, s3]
                              return ((s1,s2,s3), ob))
                          (\(s1, s2, s3) -> do
                              onNext s3 0
                              mapM_ (onNext s1) [11..20]
                              mapM_ (onNext s3) [1..10]
                              mapM_ (onNext s2) [21..30]
                              mapM_ onCompleted [s1, s2, s3])
