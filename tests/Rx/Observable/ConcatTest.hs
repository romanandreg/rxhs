module Rx.Observable.ConcatTest (tests) where

import Control.Monad (mapM_)
import qualified Rx.Subject.PublishSubject as Subject
import qualified Rx.Scheduler as Scheduler
import qualified Rx.Observable as Observable
import Rx.Types (Observable, IObserver(..))
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests = describe "concat" $ do
    it "merges in order multiple observables" $ do
      let ob1 = Observable.fromListWithScheduler
                  Scheduler.newThreadScheduler [1, 2, 3]
          ob2 = Observable.fromListWithScheduler
                  Scheduler.newThreadScheduler [4, 5, 6]
          obs = Observable.concatList [ ob2 :: Observable IO Int
                                      , ob1 :: Observable IO Int ]
      assertObservable "expecting to receive merged observables"
                       [4, 5, 6, 1, 2, 3]
                       obs
