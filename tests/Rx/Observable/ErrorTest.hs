module Rx.Observable.ErrorTest where

import qualified Rx.Observable as Observable
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests = do
  describe "onErrorReturn" $ do
    it "never emits an onError handler" $ do
      let obs = Observable.onErrorReturn 1000
                $ Observable.fromList [1, 2, 3, error "4", 5]
      assertObservable "expecting to not receive an onError"
                       [1, 2, 3, 1000]
                       obs

  describe "onErrorResumeNext" $ do
    it "never emits an onError handler" $ do
      let fallbackObs = Observable.fromList [1000, 1001]
          obs = Observable.onErrorResumeNext fallbackObs
              $ Observable.fromList [1, 2, 3, error "4", 5, 6]
      assertObservable "expecting to not receive an onError"
                       [1, 2, 3, 1000, 1001]
                       obs
