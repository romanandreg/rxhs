module Rx.Observable.SwitchTest where

import Control.Monad (mapM_)
import qualified Rx.Subject.PublishSubject as Subject
import qualified Rx.Observable as Observable
import Rx.Types (toObservable, IObserver(..))
import Rx.Test.Assertions
import Test.Hspec

tests :: Spec
tests =
  describe "switchOnNext" $ do
    it "emits events on the latest emitted observable" $ do
      assertAsyncObservable
        "expecting to receive elements in right order"
        [0,10,20,21]
        (do subjectSource <- Subject.create
            s1 <- Subject.create
            s2 <- Subject.create
            s3 <- Subject.create
            let obs = Observable.switchOnNext $ toObservable subjectSource
            return ((subjectSource, s1, s2, s3), obs))
        (\(source, s1, s2, s3) -> do
            onNext source $ toObservable s1
            onNext s1 0
            onNext source $ toObservable s2
            mapM_ (onNext s1) [2..5]
            onNext s2 10
            onNext source $ toObservable s3
            mapM_ (onNext s2) [12..16]
            onNext s3 20
            onCompleted source
            onNext s3 21
            onCompleted s3)
