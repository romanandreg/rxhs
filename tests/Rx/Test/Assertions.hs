module Rx.Test.Assertions where

import Control.Concurrent (threadDelay)
import qualified Control.Concurrent.Async as Async
import Test.HUnit (assertEqual, assertFailure)

import qualified Rx.Observable as Observable
import Rx.Observable (Observable)

assertAsyncObservable :: (Eq a, Show a)
                      => String -> [a]
                      -> (IO (s, Observable IO a))
                      -> (s -> IO ())
                      -> IO ()
assertAsyncObservable desc expected setupfn asyncfn = do
  (s, obs) <- setupfn
  listAsync <- Async.async $ Observable.toList obs
  threadDelay 100
  asyncfn s
  eResult <- Async.wait listAsync
  case eResult of
    Right result -> assertEqual desc
                                expected
                                result
    Left err -> assertFailure (show $ err)

assertAsyncObservableFailure :: (Eq a, Show a)
                             => String -> [a]
                             -> (IO (s, Observable IO a))
                             -> (s -> IO ())
                             -> IO ()
assertAsyncObservableFailure desc expected setupfn asyncfn = do
  (s, obs) <- setupfn
  listAsync <- Async.async $ Observable.toList obs
  threadDelay 100
  asyncfn s
  eResult <- Async.wait listAsync
  case eResult of
    Right result -> assertFailure "expecting observable to fail but didn't"
    Left (rem, err) -> assertEqual desc
                                   expected
                                   rem


assertObservable :: (Eq a, Show a) => String -> [a] -> Observable IO a -> IO ()
assertObservable desc expected source = do
  eResult <- Observable.toList source
  case eResult of
    Right result -> assertEqual desc
                                expected
                                result
    Left err -> assertFailure (show $ err)

assertObservableFailure :: (Eq a, Show a) => String -> [a] -> Observable IO a -> IO ()
assertObservableFailure desc expected source = do
  eResult <- Observable.toList source
  case eResult of
    Right result -> assertFailure "expecting observable to fail but didn't"
    Left (rem, err) -> assertEqual desc
                                   expected
                                   rem
