recurfind = $(shell find $(1) -name '$(2)')
HS_SOURCE_FILES = $(call recurfind, src,[^.]*.hs) $(call recurfind, bench,[^.]*.hs)

.deps:
	@echo "* Initializing cabal sandbox"
	@echo "====="
	@cabal sandbox init
	@echo "====="
	@echo ""
	@echo "* Install hlint"
	@echo ""
	@cabal install hlint
	@echo ""
	touch .deps

deps: .deps
.PHONY: deps

testloop: .deps
	@echo ""
	@echo "* Installing testloop binary"
	@echo ""
	@cabal install -ftestloop
	@echo ""
	@echo "* Running testloop"
	@echo ""
	@.cabal-sandbox/bin/rxhs-testloop
.PHONY: testloop

report.html: .deps $(HS_SOURCE_FILES) .deps
	cabal bench --benchmark-options="-o $@"

bench : report.html
.PHONY: bench
