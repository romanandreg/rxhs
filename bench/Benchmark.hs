module Main where

import Control.Monad (void)
import qualified Control.Concurrent.MVar as MVar
import Control.Concurrent (threadDelay)
import Control.Concurrent.Async (async, wait, mapConcurrently)

import Criterion.Main (bench, bgroup, defaultMain, nfIO)
import qualified Rx.Observable as Observable
import qualified Rx.Subject.SingleSubject as Subject

main :: IO ()
main = do
    subj <- Subject.create
    let mainObs = Observable.fold (+) 0 $ Observable.toObservable subj
    main_ subj mainObs
  where
    main_ subj mainObs =
        defaultMain [
            bgroup
              "Simple onNext"
              [ simpleOnNextBench 1000
              , simpleOnNextBench 10000
              , simpleOnNextBench 100000
              , simpleOnNextBench 1000000 ]
          , bgroup
              "Concurrent Simple onNext"
              [ concurrentSimpleOnNextBench 1000
              , concurrentSimpleOnNextBench 10000
              , concurrentSimpleOnNextBench 100000
              , concurrentSimpleOnNextBench 1000000 ]
          , bgroup
              "Filtered onNext"
              [ filteredOnNextBench 1000
              , filteredOnNextBench 10000
              , filteredOnNextBench 100000
              , filteredOnNextBench 1000000 ]
          ]
      where
        basicOnNext :: Int -> IO ()
        basicOnNext times = do
          mapM_ (Subject.onNext subj) [1..times]
          Subject.onCompleted subj

        concurrentOnNext nthreads times = do
          let buffer = times `div` nthreads
          void $ mapConcurrently (const $ mapM_ (Subject.onNext subj) [1..buffer])
                                 [1..nthreads]
          Subject.onCompleted subj

        runBench obs performOnNext n = bench (show n) $ do
          result <- async $ Observable.toMaybe obs
          threadDelay 100
          performOnNext n
          nfIO $ wait result

        simpleOnNextBench n =
          runBench mainObs basicOnNext n

        concurrentSimpleOnNextBench n =
          runBench mainObs (concurrentOnNext 2) n

        filteredOnNextBench n =
          let obs = Observable.filter even mainObs
          in runBench obs basicOnNext n
