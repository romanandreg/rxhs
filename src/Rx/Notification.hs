{-# LANGUAGE FlexibleContexts #-}
module Rx.Notification where

import Control.Monad.Trans (MonadIO)
import Control.Monad.Trans.Control (MonadBaseControl)

import Rx.Observer ()

import Rx.Types

getValue :: Notification v -> Maybe v
getValue (OnNext v) = Just v
getValue _ = Nothing

hasThrowable :: Notification v -> Bool
hasThrowable (OnError _) = True
hasThrowable _ = False

accept :: (MonadIO m, MonadBaseControl IO m) => Notification v -> Observer m v -> m ()
accept (OnNext v) ob = onNext ob v
accept (OnError e) ob = onError ob e
accept OnCompleted ob = onCompleted ob
