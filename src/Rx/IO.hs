{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Rx.IO where

import Control.Exception (throw)
import Control.Monad     (when)

import Data.ByteString (ByteString, hPutStr)

import           Rx.Observable (Notification (..), Observable, Observer (..),
                                Subscription)
import qualified Rx.Observable as Observable

import System.IO (BufferMode (LineBuffering), Handle, IOMode (AppendMode),
                  hClose, hIsOpen, hSetBuffering, openFile)

toHandle :: Handle -> Observable IO ByteString -> IO Subscription
toHandle handle source = do
  Observable.subscribe source
    $ Observer
    $ \case
        OnNext content -> do
          isOpen <- hIsOpen handle
          when isOpen $ do
            hPutStr handle content
        OnError err -> throw  err
        OnCompleted -> return ()

toFile :: FilePath -> Observable IO ByteString -> IO Subscription
toFile path source = do
    -- NOTE: We don't use a bracket here because
    -- writeToFile is asynchronus, and it will
    -- close the Handle before anything else gets
    -- emitted
    openFile path AppendMode >>= writeToFile
  where
    writeToFile handle = do
      hSetBuffering handle LineBuffering
      Observable.subscribe source
        $ Observer
        $ \case
            OnNext content -> do
              isOpen <- hIsOpen handle
              when isOpen $ do
                hPutStr handle content
            OnError err -> do
              hClose handle
              throw  err
            OnCompleted -> do
              hClose handle
