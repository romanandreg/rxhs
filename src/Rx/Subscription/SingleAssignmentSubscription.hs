{-# LANGUAGE RankNTypes #-}
module Rx.Subscription.SingleAssignmentSubscription
       ( module Rx.Subscription.SingleAssignmentSubscription
       , toSubscription ) where

import Control.Applicative
import Control.Concurrent.STM.TVar (TVar)

import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TVar as TVar

import Rx.Types
import qualified Rx.Subscription.Subscription as Core

empty :: IO SingleAssignmentSubscription
empty =
  (SAS . SubscriptionContainer) <$> TVar.newTVarIO Nothing

create :: Subscription -> IO SingleAssignmentSubscription
create sub =
  (SAS . SubscriptionContainer) <$> TVar.newTVarIO (Just sub)

set :: Subscription -> SingleAssignmentSubscription -> IO ()
set sub (SAS (SubscriptionContainer msubVar)) = do
  msub <- STM.atomically $ TVar.readTVar msubVar
  case msub of
    Just _ -> error "Subscription already set"
    Nothing -> STM.atomically $ TVar.writeTVar msubVar (Just sub)

get :: SingleAssignmentSubscription -> IO (Maybe Subscription)
get (SAS (SubscriptionContainer msubVar)) =
  STM.atomically $ TVar.readTVar msubVar

instance ISubscription SingleAssignmentSubscription where
  isUnsubscribed = isUnsubscribed . toSubscription
  unsubscribe = unsubscribe . toSubscription
