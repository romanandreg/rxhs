module Rx.Subscription.CompositeSubscription
       ( module Rx.Subscription.CompositeSubscription
       , toSubscription ) where

import Control.Applicative
import Control.Concurrent.STM.TVar (TVar)

import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TVar as TVar

import Rx.Types
import qualified Rx.Subscription.Subscription as Core

create :: IO CompositeSubscription
create = do
  cs <- CompositeSubscription <$> TVar.newTVarIO False
                              <*> TVar.newTVarIO []
  return $ CS cs

append :: Subscription -> CompositeSubscription -> IO ()
append s (CS cs) = do
  STM.atomically $ TVar.modifyTVar (_subscriptions cs) (s:)
