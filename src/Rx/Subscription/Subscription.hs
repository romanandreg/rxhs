module Rx.Subscription.Subscription
       ( Subscription
       , empty
       , create
       , toSubscription ) where

import Control.Applicative hiding (empty)
import Control.Monad (unless)

import Control.Concurrent.STM.TVar (TVar)

import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TVar as TVar

import Rx.Types

instance ISubscription Subscription where
  isUnsubscribed (Subscription flag _) = STM.atomically $ TVar.readTVar flag
  isUnsubscribed (CompositeSubscription flag _) = STM.atomically $ TVar.readTVar flag
  isUnsubscribed (SubscriptionContainer msubVar) = do
    msub <- STM.atomically $ TVar.readTVar msubVar
    case msub of
      Just sub -> isUnsubscribed sub
      Nothing -> return True

  unsubscribe sub@(Subscription flag action) = do
    unsubscribed <- isUnsubscribed sub
    unless unsubscribed $ do
      STM.atomically $ TVar.writeTVar flag True
      action
  unsubscribe (SubscriptionContainer msubVar) = do
    msub <- STM.atomically $ TVar.readTVar msubVar
    case msub of
      Just sub -> do
        unsubscribe sub
        STM.atomically $ TVar.writeTVar msubVar Nothing
      Nothing -> return ()
  unsubscribe (CompositeSubscription flag msubsVar) = do
    subs <- STM.atomically $ do
      TVar.writeTVar flag True
      subs <- TVar.readTVar msubsVar
      return subs
    mapM_ unsubscribe subs

empty :: IO Subscription
empty =
  Subscription <$> TVar.newTVarIO False <*> pure (return ())

create :: IO () -> IO Subscription
create action =
  Subscription <$> TVar.newTVarIO False <*> pure action
