module Rx.Subscription.BooleanSubscription
       ( module Rx.Subscription.BooleanSubscription
       , toSubscription ) where

import Control.Applicative
import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TVar as TVar
import Rx.Subscription.Subscription ()
import Rx.Types

instance ISubscription BooleanSubscription where
  isUnsubscribed = isUnsubscribed . toSubscription
  unsubscribe = unsubscribe . toSubscription

empty :: IO BooleanSubscription
empty =
  (BS . SubscriptionContainer) <$> TVar.newTVarIO Nothing

null :: BooleanSubscription -> IO Bool
null sub =
  get sub >>=  maybe (return False) (const $ return True)

create :: Subscription -> IO BooleanSubscription
create sub = do
  (BS . SubscriptionContainer) <$> TVar.newTVarIO (Just sub)

set :: Subscription -> BooleanSubscription -> IO ()
set sub (BS (SubscriptionContainer msubVar)) = do
  msub <- STM.atomically $ TVar.readTVar msubVar
  case msub of
    Nothing -> STM.atomically $ TVar.writeTVar msubVar (Just sub)
    Just prevSub -> do
      unsubscribe prevSub
      STM.atomically $ TVar.writeTVar msubVar (Just sub)

get :: BooleanSubscription -> IO (Maybe Subscription)
get (BS (SubscriptionContainer msubVar)) =
  STM.atomically $ TVar.readTVar msubVar

clear :: BooleanSubscription -> IO ()
clear sub@(BS (SubscriptionContainer msubVar)) = do
  unsubscribe sub
  STM.atomically $ TVar.writeTVar msubVar Nothing
