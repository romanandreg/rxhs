{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RankNTypes #-}
module Rx.Observable
       ( module Rx.Observable.Amb
       , module Rx.Observable.Buffer
       , module Rx.Observable.Concat
       , module Rx.Observable.Connectable
       , module Rx.Observable.Constructors
       , module Rx.Observable.Core
       , module Rx.Observable.Delay
       , module Rx.Observable.Distinct
       , module Rx.Observable.Error
       , module Rx.Observable.Map
       , module Rx.Observable.Merge
       , module Rx.Observable.Replay
       , module Rx.Observable.Scan
       , module Rx.Observable.Switch
       , module Rx.Observable.TakeUntil
       , module Rx.Observable.TakeWhile
       , module Rx.Observable.Time
       , module Rx.Observable.Timer
       , module Rx.Observable.Timeout
       , module Rx.Observable.Zip
       , Subscription
       , IObservable(..)
       , IObserver(..)
       , Notification(..)
       , Observable(..)
       , Observer(..)
       , ToObservable(..)
       , TimeoutException(..)
       , doAction
       , filter
       , filterM
       , first
       , flatMap
       , fold
       , once
       , take
       , throttle
       , toList
       , toMaybe
       , toEither
       , unsubscribe
       , subscribeAll
       , subscribeOnNext
       , subscribeOnError
       ) where

import Prelude hiding (concatMap, filter, filterM, first, map, mapM, take, takeWhile)

import Control.Lens
import Control.Applicative
import Control.Exception (SomeException(..), ErrorCall(..))
import qualified Prelude as P

import Control.Concurrent (threadDelay)
import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TBQueue as TBQueue
import qualified Control.Concurrent.STM.TVar as TVar
import qualified Control.Concurrent.MVar.Lifted as MVar
import Control.Concurrent.Lifted (killThread, fork)

import Control.Exception (SomeException)
import qualified Control.Exception.Lifted as Exception (throw)
import Control.Monad (forever, liftM, void, unless, when, forM_)
import Control.Monad.Base (MonadBase, liftBase)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)


import Data.Either (either)
import Data.Maybe (isJust)
import Data.Time.Clock (NominalDiffTime, diffUTCTime)
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Sequence as Seq
import qualified Data.Foldable as Seq (toList)

import Rx.Types

import qualified Data.Unique as Unique

import qualified Rx.Observer as Observer
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subscription.BooleanSubscription as BS
import qualified Rx.Subscription.Subscription as Subscription
import qualified Rx.Scheduler as Scheduler
import qualified Rx.Subject.PublishSubject as Subject

import Rx.Observable.Amb
import Rx.Observable.Buffer
import Rx.Observable.Concat
import Rx.Observable.Connectable
import Rx.Observable.Constructors
import Rx.Observable.Core
import Rx.Observable.Delay
import Rx.Observable.Distinct
import Rx.Observable.Error
import Rx.Observable.Map
import Rx.Observable.Merge
import Rx.Observable.Replay
import Rx.Observable.Scan
import Rx.Observable.Switch
import Rx.Observable.TakeWhile
import Rx.Observable.TakeUntil
import Rx.Observable.Time
import Rx.Observable.Timer
import Rx.Observable.Timeout
import Rx.Observable.Zip

--------------------------------------------------------------------------------

toList :: (IObservable m o, MonadIO m, MonadBaseControl IO m)
       => o a -> m (Either ([a], SomeException) [a])
toList source = do
    completedVar <- MVar.newEmptyMVar
    accVar <- liftIO $ TVar.newTVarIO Seq.empty
    sub <- subscribe source $ Observer (observerFn accVar completedVar)
    result <- MVar.takeMVar completedVar
    liftIO $ unsubscribe sub
    return result
  where
    observerFn accVar _ (OnNext v) =
      liftIO $ STM.atomically
             $ TVar.modifyTVar accVar (Seq.|> v)
    observerFn accVar completedVar (OnError err) = liftIO $ do
      acc <- STM.atomically $ TVar.readTVar accVar
      void $ MVar.tryPutMVar completedVar
           $! Left (Seq.toList acc, err)
    observerFn accVar completedVar OnCompleted = liftIO $ do
      acc <- STM.atomically $ TVar.readTVar accVar
      void $ MVar.tryPutMVar completedVar
           $! Right $ Seq.toList acc

toMaybe :: (MonadIO m, MonadBaseControl IO m)
        => Observable m a -> m (Maybe a)
toMaybe source = do
    completedVar <- MVar.newEmptyMVar
    sub <- subscribe (first source)
      $ Observer
      $ \case
          OnNext v -> MVar.putMVar completedVar (Just v)
          OnError err -> MVar.putMVar completedVar Nothing
          OnCompleted ->
             void $ MVar.tryPutMVar completedVar Nothing
    result <- MVar.takeMVar completedVar
    liftIO $ unsubscribe sub
    return result

toEither :: (MonadIO m, MonadBaseControl IO m)
         => Observable m a -> m (Either SomeException a)
toEither source = do
    completedVar <- MVar.newEmptyMVar
    sub <- subscribe (first source)
      $ Observer
      $ \case
          OnNext v ->
            MVar.putMVar completedVar (Right v)
          OnError err ->
            MVar.putMVar completedVar (Left err)
          OnCompleted ->
            void
              $ MVar.tryPutMVar completedVar
              $ Left
              $ SomeException
              $ ErrorCall "completed before getting a value"
    result <- MVar.takeMVar completedVar
    liftIO $ unsubscribe sub
    return result

--------------------------------------------------------------------------------

subscribeAll
  :: (MonadIO m, MonadBaseControl IO m, IObservable m o)
  => o a
  -> (a -> m ())
  -> (SomeException -> m ())
  -> m ()
  -> m Subscription
subscribeAll source onNextFn onErrorFn onCompletedFn =
  subscribe source
    $ Observer
    $ \case
        OnNext v -> onNextFn v
        OnError err -> onErrorFn err
        OnCompleted -> onCompletedFn

subscribeOnNext
  :: (MonadIO m, MonadBaseControl IO m, IObservable m o)
  => o a
  -> (a -> m ())
  -> m Subscription
subscribeOnNext source onNextFn =
  subscribe source
    $ Observer
    $ \case
        OnNext v -> onNextFn v
        OnError err -> Exception.throw err >> return ()
        OnCompleted -> return ()

subscribeOnError
  :: (MonadIO m, MonadBaseControl IO m, IObservable m o)
  => o a
  -> (SomeException -> m ())
  -> m Subscription
subscribeOnError source onErrorFn =
  subscribe source
    $ Observer
    $ \case
        OnNext _ -> return ()
        OnError err -> onErrorFn err
        OnCompleted -> return ()

--------------------------------------------------------------------------------

first :: (MonadIO m, MonadBaseControl IO m)
      => Observable m a -> Observable m a
first = once . take 1

once :: (MonadIO m, MonadBaseControl IO m)
     => Observable m a -> Observable m a
once source =
  Observable $ \observer -> do
    completedOrErredVar <- MVar.newMVar Nothing
    onceVar <- MVar.newMVar Nothing
    sas <- liftIO SAS.empty
    sub <-
      subscribe source
        $ Observer
        $ \case
            OnNext v -> do
              monce  <- MVar.readMVar onceVar
              case monce of
                Nothing -> do
                  MVar.swapMVar onceVar (Just v)
                  onNext observer v
                Just _ -> do
                  let err = SomeException
                              $ ErrorCall "once: expected to receive one element"
                  MVar.swapMVar completedOrErredVar (Just err)
                  onError observer err
                  liftIO $ unsubscribe sas
            OnError err -> do
              onError observer err
              liftIO $ unsubscribe sas
            OnCompleted ->
              onCompleted observer

    liftIO $ SAS.set sub sas
    return $ toSubscription sas

filterM :: (MonadIO m, MonadBaseControl IO m)
        => (v -> m Bool) -> Observable m v -> Observable m v
filterM fn =
  concatMapM $ \v -> do
    bool <- fn v
    if bool then return [v] else return []


filter :: (MonadIO m, MonadBaseControl IO m)
       => (v -> Bool) -> Observable m v -> Observable m v
filter fn = concatMap (\a -> if fn a then [a] else [])

flatMap :: (MonadIO m, MonadBaseControl IO m)
        => Observable m a -> (a -> Observable m b) -> Observable m b
flatMap observable mapfn = merge (map mapfn observable)

mapMany :: (MonadIO m, MonadBaseControl IO m)
        => Observable m a -> (a -> Observable m b) -> Observable m b
mapMany = flatMap

doAction :: (MonadIO m, MonadBaseControl IO m)
         => (a -> m ()) -> Observable m a -> Observable m a
doAction action source =
    Observable $ \observer -> do
      subscribe source
        $ Observer
        $ decorateOnNext observer
        $ \v -> action v >> onNext observer v

take :: (MonadIO m, MonadBaseControl IO m)
     => Int
     -> Observable m v
     -> Observable m v
take n source =
  Observable $ \observer ->
    if n <= 0
      then do
        onCompleted observer
        liftIO $ Subscription.empty
      else do
        sas <- liftIO $ SAS.empty
        countVar <- liftIO $ TVar.newTVarIO n
        sub <- subscribe source
          $ Observer
          $ \case
              OnNext v -> do
                count <- liftIO . atomically $ do
                  TVar.modifyTVar countVar pred
                  TVar.readTVar countVar
                if count == 0
                   then do
                     onNext observer v
                     onCompleted observer
                     liftIO $ unsubscribe $ toSubscription sas
                   else onNext observer v
              OnError err -> onError observer err
              OnCompleted -> onCompleted observer
        liftIO $ SAS.set sub sas
        return $ toSubscription sas

fold :: (MonadIO m, MonadBaseControl IO m)
     => (s -> v -> s)
     -> s
     -> Observable m v
     -> Observable m s
fold reducefn acc source =
  Observable $ \observer -> do
    stateVar <- liftIO $ TVar.newTVarIO acc
    subscribe source
      $ Observer
      $ \case
          OnNext v ->
            liftIO . atomically $ TVar.modifyTVar stateVar (flip reducefn v)
          OnError err -> do
            st <- liftIO . atomically $ TVar.readTVar stateVar
            onNext observer st
            onError observer err
          OnCompleted -> do
            st <- liftIO . atomically $ TVar.readTVar stateVar
            onNext observer st
            onCompleted observer

--------------------------------------------------------------------------------

throttleWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                      => Scheduler m
                      -> TimeInterval
                      -> Observable m a
                      -> Observable m a
throttleWithScheduler scheduler delay source =
    Observable $ \observer -> do
      mlastOnNextVar <- liftIO $ TVar.newTVarIO Nothing
      let obs = Rx.Observable.filterM (throttleFilter mlastOnNextVar)
                                      source
      subscribe obs observer
  where
    throttleFilter mlastOnNextVar v = do
      mlastOnNext <- liftIO $ STM.atomically $ TVar.readTVar mlastOnNextVar
      case mlastOnNext of
        Nothing -> do
          now <- Scheduler.now scheduler
          liftIO $ STM.atomically $ TVar.writeTVar mlastOnNextVar (Just now)
          return True
        Just backThen -> do
          now <- Scheduler.now scheduler
          let diff = diffUTCTime now backThen
              passedDelay = diff > toNominalDiffTime delay
          when passedDelay
             $ liftIO
             $ STM.atomically
             $ TVar.writeTVar mlastOnNextVar (Just now)
          return passedDelay

throttle :: (MonadIO m, MonadBaseControl IO m)
         => TimeInterval
         -> Observable m a
         -> Observable m a
throttle =
  throttleWithScheduler Scheduler.immediateScheduler

--------------------------------------------------------------------------------

instance (MonadIO m, MonadBaseControl IO m) => Monad (Observable m) where
  return = just
  (>>=) = flatMap
