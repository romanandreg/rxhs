{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Rx.Observer
       ( module Rx.Observer
       , Notification(..)
       , Observer(..) ) where

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar (newTVarIO, readTVar, writeTVar)

import Control.Exception (SomeException)
import Control.Monad (unless)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl(..), control)
import qualified Control.Exception.Lifted as Ex

import Rx.Types

--------------------------------------------------------------------------------

instance MonadBaseControl IO m => IObserver m (Observer m) where
  emitNotification (Observer observerFn) notification = observerFn notification
  onNext (Observer observerFn) v = observerFn (OnNext v)
  onError (Observer observerFn) err = observerFn (OnError err)
  onCompleted (Observer observerFn) = observerFn OnCompleted

--------------------------------------------------------------------------------

safe :: (ISubscription s, MonadIO m, MonadBaseControl IO m)
     => s -> Observer m v -> m (Observer m v)
safe subscription observer = do
    isCompletedVar <- liftIO $ newTVarIO False
    return $! Observer $ safeDecoration isCompletedVar
  where
    safeDecoration isCompletedVar (OnNext v) = do
      isCompleted <- liftIO $ atomically $ readTVar isCompletedVar
      unless isCompleted $ do
        result <- Ex.try (v `seq` onNext observer v)
        case result of
          Left err -> safeDecoration isCompletedVar (OnError err)
          Right _ -> return ()
    safeDecoration isCompletedVar (OnError err) = do
      isCompleted <- liftIO $ atomically $ readTVar isCompletedVar
      unless isCompleted $
        Ex.finally (onError observer err)
                   (liftIO $ do
                       unsubscribe subscription
                       atomically $ writeTVar isCompletedVar True)
    safeDecoration isCompletedVar OnCompleted = do
      isCompleted <- liftIO $ atomically $ readTVar isCompletedVar
      unless isCompleted $ do
        result <- Ex.try $ onCompleted observer
        case result of
          Left err -> safeDecoration isCompletedVar (OnError err)
          Right _ -> liftIO $ do
            atomically $ writeTVar isCompletedVar True
            -- unsubscribe subscription
