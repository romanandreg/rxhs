{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Subject.SingleSubject (
    IObservable (..)
  , IObserver (..)
  , Subject
  , create
  , createWithQueue
  ) where

import Control.Applicative

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar (TVar)
import qualified Control.Concurrent.STM.TVar as TVar
import qualified Control.Concurrent.STM.TQueue as TQueue

import Rx.Observable.Core ()
import qualified Rx.Notification as Notification
import qualified Rx.Subscription.Subscription as Subscription
import Rx.Types

--------------------------------------------------------------------------------

create_ :: (MonadIO m, MonadBaseControl IO m)
        => Bool -> m (Subject m v)
create_ queueOnEmpty = do
    observerVar <- liftIO $ TVar.newTVarIO Nothing
    completedVar <- liftIO $ TVar.newTVarIO False
    notificationQueue <- liftIO TQueue.newTQueueIO
    main observerVar completedVar notificationQueue
  where
    main observerVar completedVar notificationQueue =
        return $ Subject singleSubscribe singleEmitNotification
      where
        emitQueuedNotifications observer = when queueOnEmpty $ do
          result <-
            liftIO . atomically $ TQueue.tryReadTQueue notificationQueue
          case result of
            Just notification -> do
              Notification.accept notification observer
              emitQueuedNotifications observer
            Nothing -> return ()

        acceptNotification notification = do
          result <- liftIO . atomically $ TVar.readTVar observerVar
          case result of
            Nothing
              | queueOnEmpty ->
                  liftIO . atomically $
                    TQueue.writeTQueue notificationQueue notification
              | otherwise -> return ()
            Just observer ->
              Notification.accept notification observer

        singleSubscribe observer = do
           completed <- liftIO . atomically $ TVar.readTVar completedVar
           if completed
             then do
               Notification.accept OnCompleted observer
               liftIO Subscription.empty
             else do
               prevObserver <- liftIO . atomically $ TVar.readTVar observerVar
               maybe (emitQueuedNotifications observer) onCompleted prevObserver
               liftIO . atomically
                 $ TVar.modifyTVar observerVar (const $ Just observer)
               liftIO
                 $ Subscription.create
                 $ atomically $ TVar.writeTVar observerVar Nothing

        singleEmitNotification (OnError err) = do
          liftIO . atomically $ TVar.writeTVar completedVar True
          acceptNotification $ OnError err

        singleEmitNotification OnCompleted = do
          liftIO . atomically $ TVar.writeTVar completedVar True
          acceptNotification OnCompleted

        singleEmitNotification notification =
          acceptNotification notification

--------------------------------------------------------------------------------

create :: (MonadIO m, MonadBaseControl IO m) => m (Subject m v)
create = create_ False

createWithQueue :: (MonadIO m, MonadBaseControl IO m) => m (Subject m v)
createWithQueue = create_ True
