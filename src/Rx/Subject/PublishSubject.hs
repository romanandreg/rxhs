{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Subject.PublishSubject (
    IObservable (..)
  , IObserver (..)
  , Subject
  , create
  ) where

import Control.Applicative

import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar

import qualified Data.Unique as Unique
import qualified Data.HashMap.Strict as HashMap

import Rx.Observable.Core ()
import qualified Rx.Notification as Notification
import qualified Rx.Subscription.Subscription as Subscription
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import Rx.Types

--------------------------------------------------------------------------------

create :: (MonadIO m, MonadBaseControl IO m) => m (Subject m v)
create = do
    subscriptionsVar <- liftIO $ TVar.newTVarIO HashMap.empty
    completedVar <- liftIO $ TVar.newTVarIO False
    main subscriptionsVar completedVar
  where
    main subscriptionsVar completedVar =
        return $ Subject psSubscribe psEmitNotification
      where
        getCompleted = liftIO . atomically $ TVar.readTVar completedVar
        getSubscriptions = liftIO . atomically $ TVar.readTVar subscriptionsVar
        psSubscribe observer = do
          completed <- getCompleted
          if completed
            then do
              Notification.accept OnCompleted observer
              liftIO Subscription.empty
            else liftIO $ do
              subscriptionId <- Unique.hashUnique <$> Unique.newUnique
              subscription <- Subscription.create $
                atomically $ TVar.modifyTVar subscriptionsVar
                                            (HashMap.delete subscriptionId)

              atomically $ TVar.modifyTVar subscriptionsVar
                                           (HashMap.insert subscriptionId observer)

              return subscription


        psEmitNotification notification = do
          subscriptions <- getSubscriptions
          mapM_ (Notification.accept notification)
                (HashMap.elems subscriptions)
