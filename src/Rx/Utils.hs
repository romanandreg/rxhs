module Rx.Utils where

import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TVar as TVar

compareAndSet :: Eq a => TVar.TVar a -> a -> a -> IO a
compareAndSet tvar expected update =
  STM.atomically $ do
    TVar.modifyTVar tvar (\v -> if v == expected
                                then update
                                else expected)
    TVar.readTVar tvar
