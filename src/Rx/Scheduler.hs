{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
module Rx.Scheduler
       ( Scheduler
       , now
       , relativeTime
       , absoluteTime
       , immediateScheduler
       , newThreadScheduler
       , createSingleThreadScheduler
       , schedule
       , scheduleWithState
       , scheduleRecursive
       , scheduleRecursiveWithState
       , scheduleTimed
       -- , scheduleTimedWithState
       , scheduleTimedRecursive
       , scheduleTimedRecursiveState
       , schedule'
       , scheduleWithState'
       , scheduleRecursive'
       , scheduleRecursiveWithState'
       , scheduleTimed'
       , scheduleTimedWithState'
       , scheduleTimedRecursive'
       , scheduleTimedRecursiveState'
       ) where

import           Control.Concurrent           (ThreadId, forkIO, killThread,
                                               myThreadId, threadDelay)
import           Control.Concurrent.STM       (atomically)
import qualified Control.Concurrent.STM.TChan as TChan

import Control.Monad       (forever, void)
import Control.Monad.Trans (MonadIO (..))
import Control.Monad.Trans.Control (MonadBaseControl)
import Data.Time.Clock     (NominalDiffTime, UTCTime, diffUTCTime,
                            getCurrentTime)

import Control.Concurrent.Lifted (fork)


import qualified Rx.Subscription.Subscription as Subscription
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subscription.BooleanSubscription as BS

import Rx.Types

--------------------------------------------------------------------------------

now :: MonadIO m => Scheduler m -> m UTCTime
now = _now

relativeTime :: DueTime -> ScheduledTime
relativeTime = ScheduleRelative

absoluteTime :: UTCTime -> ScheduledTime
absoluteTime = ScheduleAbsolute

--------------------------------------------------------------------------------

invokeAction :: Monad m => Scheduler m -> s -> Step s m Subscription -> m Subscription
invokeAction _ st action = action st

invokeRecImmediate :: MonadIO m
                   => Scheduler m
                   -> (s, RecursiveStep s m Subscription)
                   -> m Subscription
invokeRecImmediate scheduler (st0, action) = do
    cs <- liftIO $ CS.create
    bs <- liftIO $ BS.empty

    actionSubscription <- recursiveAction bs st0

    liftIO $ CS.append (toSubscription bs) cs
    liftIO $ CS.append actionSubscription cs

    return $ toSubscription cs
  where
    recursiveAction bs st = action st $ recursiveStep bs
    recursiveStep bs st = do
      d <- scheduleWithState' scheduler st $ recursiveAction bs
      liftIO $ BS.set d bs
      return d

invokeRecDate :: MonadIO m
              => Scheduler m
              -> RelativeSchedulerFn s m
              -> (s, RecursiveStep s m Subscription)
              -> m Subscription
invokeRecDate scheduler scheduleFn (st0, action) = do
    cs <- liftIO $ CS.create
    bs <- liftIO $ BS.empty

    actionSubscription <- recursiveAction bs st0

    liftIO $ CS.append (toSubscription bs) cs
    liftIO $ CS.append actionSubscription cs

    return $ toSubscription cs
  where
    recursiveAction bs st = action st $ recursiveStep1 bs
    recursiveStep1 bs st2 = do
      sub <- scheduleFn scheduler st2 (recursiveStep2 bs)
      liftIO $ BS.set sub bs
      return sub
    recursiveStep2 bs st3 = do
      recursiveAction bs st3

-- --------------------------------------------------------------------------------

immediateScheduler :: MonadIO m => Scheduler m
immediateScheduler =
  Scheduler {
      _now      = liftIO getCurrentTime
    , _schedule = invokeAction immediateScheduler
    , _scheduleTimed = _scheduleTimed'
    }
  where
    _scheduleTimed' st (ScheduleRelative dueTime) action
      | dueTime > 0 = fail "Scheduler is not allowed to block thread"
      | otherwise = invokeAction immediateScheduler st $
                      \st' -> action st'
    _scheduleTimed' st (ScheduleAbsolute time) action = do
      now <- liftIO $ getCurrentTime
      let dueTime = diffUTCTime time now
      _scheduleTimed' st (toScheduledTime dueTime) action


-- --------------------

newThreadScheduler :: (MonadIO m, MonadBaseControl IO m) => Scheduler m
newThreadScheduler =
    Scheduler {
      _now = liftIO getCurrentTime
    , _schedule = _schedule'
    , _scheduleTimed = _scheduleTimed'
    }
  where
    scheduleThread action = do
      cs <- liftIO $ CS.create
      sas <- liftIO $ SAS.empty
      tid <- fork $ do
        d <- action
        liftIO $ SAS.set d sas
      threadSub <- liftIO $ Subscription.create $ killThread tid
      liftIO $ CS.append (toSubscription sas) cs
      liftIO $ CS.append threadSub cs
      return $ toSubscription cs

    _schedule' st action = scheduleThread (action st)
    _scheduleTimed' st time@(ScheduleRelative dueTime) action = scheduleThread $ do
      liftIO $ threadDelay dueTime
      action st
    _scheduleTimed' st (ScheduleAbsolute datetime) action  = do
      now <- liftIO $ getCurrentTime
      _scheduleTimed' st (toScheduledTime $ diffUTCTime datetime now) action

-- --------------------

createSingleThreadScheduler :: (MonadIO m, MonadBaseControl IO m)
                            => m (Scheduler m, Subscription)
createSingleThreadScheduler = do
    cs <- liftIO $ CS.create
    bs <- liftIO $ BS.empty
    taskQueue <- liftIO TChan.newTChanIO
    tid <- fork $ forever $ do
      action <- liftIO $ atomically $ TChan.readTChan taskQueue
      void $ action

    threadSub <- liftIO $ Subscription.create $ do
      killThread tid

    liftIO $ CS.append (toSubscription bs) cs
    liftIO $ CS.append threadSub cs

    let scheduler = Scheduler {
      _now = liftIO getCurrentTime
    , _schedule = _schedule' bs taskQueue
    , _scheduleTimed = _scheduleTimed' bs taskQueue
    }

    return (scheduler, toSubscription cs)
  where
    _schedule' bs taskQueue st action = liftIO $ do
      atomically $ TChan.writeTChan taskQueue (action st)
      return $ toSubscription bs
    _scheduleTimed' bs taskQueue st time@(ScheduleRelative dueTime) action = liftIO $ do
      threadDelay dueTime
      atomically $ TChan.writeTChan taskQueue (action st)
      return $ toSubscription bs
    _scheduleTimed' bs taskQueue st (ScheduleAbsolute dueTime) action = undefined


-- --------------------------------------------------------------------------------

schedule' :: MonadIO m => Scheduler m -> m Subscription -> m Subscription
schedule' (Scheduler {..}) action =
  _schedule () $ const action

scheduleWithState' :: MonadIO m
                   => Scheduler m -> s -> Step s m Subscription -> m Subscription
scheduleWithState' (Scheduler {..}) = _schedule

scheduleTimed' :: (MonadIO m, ToScheduledTime t)
               => Scheduler m -> t -> Step () m Subscription -> m Subscription
scheduleTimed' (Scheduler {..}) t action =
  _scheduleTimed () (toScheduledTime t) (const $ action ())

scheduleTimedWithState' :: (MonadIO m, ToScheduledTime t)
                        => t -> Scheduler m -> s
                        -> Step s m Subscription -> m Subscription
scheduleTimedWithState' t (Scheduler {..}) st =
  _scheduleTimed st (toScheduledTime t)

--

scheduleRecursive' :: MonadIO m
                   => Scheduler m
                   -> (m Subscription -> m Subscription)
                   -> m Subscription
scheduleRecursive' scheduler action = do
  scheduleRecursiveWithState' scheduler () $
    \_ step -> action (step ())

scheduleRecursiveWithState' :: MonadIO m
                            => Scheduler m
                            -> s
                            -> (s -> (s -> m Subscription) -> m Subscription)
                            -> m Subscription
scheduleRecursiveWithState' scheduler st action = do
  scheduleWithState' scheduler (st, action) $
    invokeRecImmediate scheduler

scheduleTimedRecursive' :: (MonadIO m, ToScheduledTime t)
                        => Scheduler m
                        -> t
                        -> (m Subscription -> m Subscription)
                        -> m Subscription
scheduleTimedRecursive' scheduler dueTime0 action =
    scheduleTimedRecursiveState' scheduler () dueTime0 $
      \_ step -> action (step ())


scheduleTimedRecursiveState' :: (MonadIO m, ToScheduledTime t)
                             => Scheduler m
                             -> s
                             -> t
                             -> RecursiveStep s m Subscription
                             -> m Subscription
scheduleTimedRecursiveState' scheduler@(Scheduler {..}) st0 time action =
  _scheduleTimed (st0, action) (toScheduledTime time) $
    invokeRecDate scheduler (scheduleTimedWithState' time)

-- --------------------------------------------------------------------------------

schedule :: (Functor m, MonadIO m) => Scheduler m -> m () -> m Subscription
schedule scheduler action =
  schedule' scheduler (action >> liftIO Subscription.empty)

scheduleWithState :: (Functor m, MonadIO m)
                  => Scheduler m -> s -> (s -> m ()) -> m Subscription
scheduleWithState scheduler st action =
  scheduleWithState' scheduler st (\st' -> action st' >> liftIO Subscription.empty)


scheduleTimed :: (MonadIO m, ToScheduledTime t)
              => Scheduler m
              -> t -> m () -> m Subscription
scheduleTimed scheduler t action = do
  scheduleTimed' scheduler t (\_ -> action >> liftIO Subscription.empty)

scheduleRecursive :: (Functor m, MonadIO m)
                  => Scheduler m
                  -> (m () -> m ())
                  -> m Subscription
scheduleRecursive scheduler action = do
  scheduleRecursive' scheduler $ (>> liftIO Subscription.empty) . action . void

scheduleRecursiveWithState :: (Functor m, MonadIO m)
                           => Scheduler m
                           -> s
                           -> (s -> (s -> m ()) -> m ())
                           -> m Subscription
scheduleRecursiveWithState scheduler st0 action =
    scheduleRecursiveWithState' scheduler st0 action'
  where
    action' st step = action st (void . step) >> liftIO Subscription.empty

--

scheduleTimedRecursive :: (Functor m, MonadIO m, ToScheduledTime t)
                       => Scheduler m
                       -> t
                       -> (m () -> m ())
                       -> m Subscription
scheduleTimedRecursive scheduler time action =
    scheduleTimedRecursive' scheduler (toScheduledTime time) $
      \step -> action (void $ step) >> liftIO Subscription.empty


scheduleTimedRecursiveState :: (Functor m, MonadIO m, ToScheduledTime t)
                            => Scheduler m
                            -> s
                            -> t
                            -> (s -> Step s m () -> m ())
                            -> m Subscription
scheduleTimedRecursiveState scheduler@(Scheduler {..}) st0 time action =
    scheduleTimedRecursiveState' scheduler st0 (toScheduledTime time) $
      \st step -> action st (void . step) >> liftIO Subscription.empty
