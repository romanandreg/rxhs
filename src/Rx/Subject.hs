{-# LANGUAGE FlexibleContexts #-}
module Rx.Subject (
    Subject
  , IObserver(..)
  , IObservable(..)
  , publishSubject
  , singleSubject
  , singleSubjectWithQueue
  , toObservable ) where

import qualified Rx.Subject.PublishSubject as Publish
import qualified Rx.Subject.SingleSubject as Single
import Rx.Types

--------------------------------------------------------------------------------

publishSubject :: (MonadIO m, MonadBaseControl IO m)
               => m (Subject m v)
publishSubject = Publish.create

singleSubject :: (MonadIO m, MonadBaseControl IO m)
              => m (Subject m v)
singleSubject = Single.create

singleSubjectWithQueue :: (MonadIO m, MonadBaseControl IO m)
                       => m (Subject m v)
singleSubjectWithQueue = Single.createWithQueue
