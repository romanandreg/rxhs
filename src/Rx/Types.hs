{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Rx.Types
       ( module Rx.Types
       , module Control.Applicative
       , MonadIO(..)
       , MonadBaseControl(..)
       , TimeInterval
       , atomically
       , toNominalDiffTime
       , toSeconds
       , when
       , unless
       ) where

import Control.Applicative ((<$>), (<*>))
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar (TVar)
import Control.Exception (Exception(..), SomeException)

import Control.Monad (when, unless)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl(..))

import Data.Typeable (Typeable)
import Data.Sequence (Seq)
import Data.HashMap.Strict (HashMap)
import Data.Time.Clock (UTCTime, NominalDiffTime)

import Tiempo (TimeInterval, toMicroSeconds, toSeconds, toNominalDiffTime)

--------------------------------------------------------------------------------

class ISubscription a where
  unsubscribe :: a -> IO ()
  isUnsubscribed :: a -> IO Bool

class ToSubscription s where
  toSubscription :: s -> Subscription

data Subscription
  = Subscription {
    _isUnsubscribed :: TVar Bool
  , _unsubscribe :: IO ()
  }
  | SubscriptionContainer {
    _currentSubscription :: TVar (Maybe Subscription)
  }
  | CompositeSubscription {
    _isUnsubscribed :: TVar Bool
  , _subscriptions :: TVar [Subscription]
  }

instance ToSubscription Subscription where
  toSubscription = id

newtype SingleAssignmentSubscription
  = SAS Subscription
  deriving (ToSubscription)

newtype CompositeSubscription
  = CS Subscription
  deriving (ToSubscription)

newtype BooleanSubscription
  = BS Subscription
  deriving (ToSubscription)

--------------------------------------------------------------------------------

newtype Observer m v = Observer (Notification v -> m ())

class IObserver m o where
  emitNotification :: o v -> Notification v -> m ()
  onNext :: o v -> v -> m ()
  onNext observer v = emitNotification observer (OnNext v)
  onError :: o v -> SomeException -> m ()
  onError observer err = emitNotification observer (OnError err)
  onCompleted :: o v -> m ()
  onCompleted observer = emitNotification observer OnCompleted

--------------------------------------------------------------------------------

data Observable m v =
  Observable {
    observableOnSubscribe :: Observer m v -> m Subscription
  }

class IObservable m o where
  subscribe :: o v -> Observer m v -> m Subscription

class ToObservable o where
  toObservable :: o m v -> Observable m v

data GenericObservable m v
  = forall o. IObservable m o => GenericObservable (o v)

--------------------------------------------------------------------------------

data Notification v
  = OnNext v
  | OnError SomeException
  | OnCompleted
  deriving (Show)

data Subject m v =
  Subject {
    _subjectOnSubscribe        :: Observer m v -> m Subscription
  , _subjectOnEmitNotification :: Notification v -> m ()
  }

--------------------------------------------------------------------------------

data Replayer m v
  = Replayer {
    _replayerAddItem      :: Notification v -> m ()
  , _replayerReplayAction :: m ()
  }

emptyReplayer :: (MonadIO m, MonadBaseControl IO m) => Replayer m v
emptyReplayer = Replayer (const $ return ()) (return ())

--------------------------------------------------------------------------------

data ConnectableObservable m v
  = ConnectableObservable {
    _connectableIsConnected        :: TVar Bool
  , _connectableSubject            :: Subject m v
  , _connectableSourceSubscription :: Subscription
  , _connectableConnectedBuffer    :: TVar (Seq v)
  , _connectableReplayerVar        :: TVar (Replayer m v)
  }

--------------------------------------------------------------------------------

data TimeoutException
  = TimeoutException TimeInterval
  deriving (Show, Typeable)

instance Exception TimeoutException

--------------------------------------------------------------------------------

type DueTime = Int

data ScheduledTime
  = ScheduleRelative DueTime
  | ScheduleAbsolute UTCTime

class ToScheduledTime d where
  toScheduledTime :: d -> ScheduledTime

instance ToScheduledTime ScheduledTime where
  toScheduledTime = id

instance ToScheduledTime Int where
  toScheduledTime = ScheduleRelative

instance ToScheduledTime UTCTime where
  toScheduledTime = ScheduleAbsolute

instance ToScheduledTime NominalDiffTime where
  toScheduledTime diffTime =
      toScheduledTime $ toMicroSeconds diffTime
    where
      toMicroSeconds :: NominalDiffTime -> DueTime
      toMicroSeconds = fromIntegral . micros
      micros :: NominalDiffTime -> DueTime
      micros = floor . (* microUnit) . toRational
      microUnit = 10 ^ (6 :: Integer)

instance ToScheduledTime TimeInterval where
  toScheduledTime interval =
    ScheduleRelative $! toMicroSeconds interval

--

type Step s m out = s -> m out
type RecursiveStep s m out = s -> Step s m out -> m out


type RelativeSchedulerFn s m =
  Scheduler m -> s -> Step s m Subscription -> m Subscription

--

data Scheduler m
  = Scheduler {
    _now           :: m UTCTime
  , _schedule      :: forall s. s
                   -> Step s m Subscription -> m Subscription
  , _scheduleTimed :: forall s. s
                   -> ScheduledTime -> Step s m Subscription -> m Subscription
  }
