{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Constructors where

import Control.Exception (SomeException)
import qualified Rx.Scheduler as Scheduler
import qualified Rx.Subscription.Subscription as Subscription
import qualified Rx.Subscription.CompositeSubscription as CS
import Rx.Observer ()
import Rx.Types

never :: MonadIO m => Observable m v
never = Observable $ \_ -> liftIO $ Subscription.empty

throw :: (MonadIO m, MonadBaseControl IO m) => SomeException -> Observable m v
throw err = Observable $ \observer -> do
  onError observer err
  liftIO $ Subscription.empty

fromListWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                      => Scheduler m
                      -> [v]
                      -> Observable m v
fromListWithScheduler scheduler vs = Observable $ \observer ->
  Scheduler.schedule scheduler $ do
    mapM_ (onNext observer) vs
    onCompleted observer

fromList :: (MonadIO m, MonadBaseControl IO m) => [v] -> Observable m v
fromList = fromListWithScheduler Scheduler.immediateScheduler

range :: (Enum v, MonadIO m, MonadBaseControl IO m)
      => v -> v -> Observable m v
range start end = fromList [start..end]

just :: (MonadIO m, MonadBaseControl IO m) => v -> Observable m v
just v = fromList [v]
