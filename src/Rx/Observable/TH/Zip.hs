{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.TH.Zip where

import Language.Haskell.TH

import Control.Exception (SomeException)

import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TBQueue as TBQueue
import qualified Control.Concurrent.STM.TVar as TVar

import Control.Monad (forM, unless, when)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)

import qualified Rx.Subscription.CompositeSubscription as CS
import Rx.Utils (compareAndSet)
import Rx.Types


foo :: IO [Dec]
foo = runQ [d|

  zipWith :: (MonadIO m, MonadBaseControl IO m)
          => (a -> b -> c)
          -> Observable m a -> Observable m b -> Observable m c
  zipWith zipfn source1 source2 =
      Observable $ \observer -> do
        cs <- liftIO $ CS.create
        q1 <- liftIO $ TBQueue.newTBQueueIO 100
        q2 <- liftIO $ TBQueue.newTBQueueIO 100
        completedVar      <- liftIO $ TVar.newTVarIO False
        completedCountVar <- liftIO $ TVar.newTVarIO 0
        subscriptionsVar  <- liftIO $ TVar.newTVarIO []
        let allQueues = (q1, q2)

        zipObservers subscriptionsVar
                     completedVar
                     completedCountVar
                     (source1, source2)
                     (q1, q2)
                     observer
    where
      putNextElement queue = liftIO . STM.atomically . TBQueue.writeTBQueue queue
      getNextElement = liftIO . STM.atomically . TBQueue.readTBQueue
      isEmptyQueue   = liftIO . STM.atomically . TBQueue.isEmptyTBQueue
      zipObservers subscriptionsVar
                   completedVar
                   completedCountVar
                   allSources@(s1, s2)
                   allQueues@(q1, q2)
                   observer = do

          cs <- liftIO $ CS.create
          sub1 <- subscribe s1 $ Observer observerFn1
          sub2 <- subscribe s2 $ Observer observerFn2

          liftIO $ do
            STM.atomically $ TVar.writeTVar subscriptionsVar [sub1, sub2]
            CS.append sub1 cs
            CS.append sub2 cs

          return $ toSubscription cs
        where
          stopSubscriptions = liftIO $ do
            wasCompleted <- compareAndSet completedVar False True
            when wasCompleted $
              STM.atomically (TVar.readTVar subscriptionsVar) >>=
              mapM_ unsubscribe
          onNext' = do
            q1IsEmpty <- isEmptyQueue q1
            q2IsEmpty <- isEmptyQueue q2
            unless (q1IsEmpty || q2IsEmpty) $ do
              val1 <- getNextElement q1
              val2 <- getNextElement q2
              let result = zipfn val1 val2
              onNext observer result
          onCompleted' = do
            completedCount <- liftIO $ STM.atomically $ do
              TVar.modifyTVar completedCountVar (+1)
              TVar.readTVar completedCountVar
            when (completedCount == 2) $ do
              stopSubscriptions
              onCompleted observer
          onError' err = do
            stopSubscriptions
            onError observer err
          observerFn1 (OnNext v1) = putNextElement q1 v1 >> onNext'
          observerFn1 OnCompleted = onCompleted'
          observerFn1 (OnError err) = onError' err
          observerFn2 (OnNext v2) = putNextElement q2 v2 >> onNext'
          observerFn2 OnCompleted = onCompleted'
          observerFn2 (OnError err) = onError' err |]

--------------------------------------------------------------------------------

isEmptyQueue :: MonadIO m => TBQueue.TBQueue a -> m Bool
isEmptyQueue   = liftIO . STM.atomically . TBQueue.isEmptyTBQueue

putNextElement :: MonadIO m => TBQueue.TBQueue a -> a -> m ()
putNextElement queue = liftIO . STM.atomically . TBQueue.writeTBQueue queue

getNextElement :: MonadIO m => TBQueue.TBQueue a -> m a
getNextElement = liftIO . STM.atomically . TBQueue.readTBQueue

isEmptyQueueName :: Name -> Name
isEmptyQueueName queueName = mkName $ nameBase queueName ++ "IsEmpty"

nextQueueElementName :: Name -> Name
nextQueueElementName queueName = mkName $ nameBase queueName ++ "Val"

stopSubscriptions subscriptionsVar completedVar = liftIO $ do
  wasCompleted <- compareAndSet completedVar False True
  when wasCompleted $
    STM.atomically (TVar.readTVar subscriptionsVar) >>= mapM_ unsubscribe

--------------------------------------------------------------------------------

onErrorZip :: (IObserver m o, ISubscription a, MonadIO m)
           => TVar.TVar [a]
           -> TVar.TVar Bool
           -> o v
           -> SomeException
           -> m ()
onErrorZip subscriptionsVar completedVar observer err = do
  stopSubscriptions subscriptionsVar completedVar
  onError observer err

--------------------------------------------------------------------------------

onCompletedZip :: (IObserver m o, ISubscription s, MonadIO m)
               => Int
               -> TVar.TVar [s]
               -> TVar.TVar Bool
               -> TVar.TVar Int
               -> o v -> m ()
onCompletedZip queueCount subscriptionsVar completedVar completedCountVar observer = do
  completedCount <- liftIO $ STM.atomically $ do
    TVar.modifyTVar completedCountVar (+1)
    TVar.readTVar completedCountVar
  when (completedCount == queueCount) $ do
    stopSubscriptions subscriptionsVar completedVar
    onCompleted observer

makeOnCompletedFn :: Integer -> Q Dec
makeOnCompletedFn queueCount = do
    funD (mkName $ "onCompletedZip" ++ show queueCount)
         [clause
          [ varP subscriptionsVarName
          , varP completedVarName
          , varP completedCountVarName
          , varP observerName ]
          (normalB ([| onCompletedZip |] `appE`
                    (litE (IntegerL queueCount)) `appE`
                    (varE subscriptionsVarName) `appE`
                    (varE completedVarName) `appE`
                    (varE completedCountVarName) `appE`
                    (varE observerName)))
          []]
  where
    subscriptionsVarName = mkName "subscriptionsVar"
    completedVarName = mkName "completedVar"
    completedCountVarName = mkName "completedCountVar"
    observerName = mkName "observer"

--------------------------------------------------------------------------------


makeOnNextFn :: Integer -> Q Dec
makeOnNextFn queueCount =
    funD (mkName $ "onNextZip" ++ show queueCount)
         [clause
           [ tupP $ map varP queueNames, varP zipFnName, varP observerName ]
           (normalB
            (doE $ map makeQueueIsEmptyStmt queueNames
                ++ [noBindS
                    ([| unless |] `appE`
                    (appE [| or |] $ listE
                                   $ map (varE . isEmptyQueueName) queueNames) `appE`
                    (doE $ map makeGetNextElementStmt queueNames
                         ++ [letS [valD (varP zippedValue)
                                        (normalB (applyZipFn queueNames))
                                      []]
                            , noBindS ([| onNext |]  `appE`
                                       varE observerName `appE`
                                       varE zippedValue)]))]))
                []]
  where
    queueNames   = map (mkName . ("q" ++) . show) [1..queueCount]
    zipFnName    = mkName "zipfn"
    observerName = mkName "observer"
    zippedValue  = mkName "zippedValue"

    isEmptyQueueFn   = [| isEmptyQueue |]
    getNextElementFn = [| getNextElement |]

    makeQueueIsEmptyStmt queueName =
        bindS (varP $ isEmptyQueueName queueName)
              (appE isEmptyQueueFn $ varE queueName)

    makeGetNextElementStmt queueName =
        bindS (varP $ nextQueueElementName queueName)
              (appE getNextElementFn $ varE queueName)

    applyZipFn = return .
                 applyZipFn' .
                 reverse .
                 map (VarE . nextQueueElementName)
      where
        applyZipFn' [n] =
          AppE (VarE (mkName "zipfn")) n
        applyZipFn' (n:queueNames) =
          AppE (applyZipFn' queueNames) n

--------------------------------------------------------------------------------

makeZipObserverFn :: Integer -> Q Dec
makeZipObserverFn nSources = do
  funD
    (mkName $ "zipObserver" ++ show nSources)
    [ clause
        [ varP $ mkName "zipfn"
        , varP $ mkName "subscriptionsVar"
        , varP $ mkName "completedVar"
        , varP $ mkName "completedCountVar"
        , asP (mkName "allSources")
              (tupP $ map (varP . mkName . ("s" ++) . show) [1..nSources])
        , asP (mkName "allQueues")
              (tupP $ map (varP . mkName . ("q" ++) . show) [1..nSources])
        , varP $ mkName "observer" ]
        (normalB
          (doE
           ([ bindS (varP $ mkName "cs") [|  liftIO CS.create |] ]
            ++ map subStmt [1..nSources]
            ++ [ noBindS ([| liftIO |] `appE`
                         (doE $
                          [noBindS
                            [| STM.atomically
                                 $ TVar.writeTVar $(varE $ mkName "subscriptionsVar")
                                 $ $(listE (map (varE . mkName . ("sub" ++) . show)
                                                [1..nSources])) |] ]
                           ++ map addDispStmt [1..nSources]))
               , noBindS [| return $ toSubscription $(varE $ mkName "cs") |]])))
        [] ]
  where
    addDispStmt i =
      noBindS [| CS.append $(varE $ mkName $ "sub" ++ show i)
                           $(varE $ mkName "cs") |]
    subStmt i =
      bindS (varP $ mkName $ "sub" ++ show i)
            ( [| subscribe |] `appE`
              (varE $ mkName $ "s" ++ show i) `appE`
              ([| Observer |] `appE`
                (lamCaseE
                  [ match
                      (conP (mkName "OnNext")
                            [varP $ mkName "v"])
                      (normalB
                        (doE
                          [ noBindS ([|putNextElement|] `appE`
                                     (varE $ mkName $ "q" ++ show i) `appE`
                                     (varE $ mkName "v"))
                          , noBindS ((varE $ mkName $ "onNextZip" ++ show nSources) `appE`
                                     (varE $ mkName "allQueues") `appE`
                                     (varE $ mkName "zipfn") `appE`
                                     (varE $ mkName "observer")) ]))
                      []
                  , match
                     (conP (mkName "OnCompleted") [])
                     (normalB
                       (doE [noBindS
                              ((varE $ mkName $ "onCompletedZip" ++ show nSources) `appE`
                               (varE $ mkName "subscriptionsVar") `appE`
                               (varE $ mkName "completedVar") `appE`
                               (varE $ mkName "completedCountVar") `appE`
                               (varE $ mkName "observer"))]))
                     []
                  , match
                     (conP (mkName "OnError") [varP $ mkName "err"])
                     (normalB ([| onErrorZip |] `appE`
                               (varE $ mkName "subscriptionsVar") `appE`
                               (varE $ mkName "completedVar") `appE`
                               (varE $ mkName "observer") `appE`
                               (varE $ mkName "err")))
                     []])))

--------------------------------------------------------------------------------

makeZipWith :: Integer -> Q [Dec]
makeZipWith nSources = do
  zipWithSig <- makeZipWithSig nSources
  zipWithFn  <- makeZipWithFn nSources
  return $ [zipWithSig, zipWithFn]

makeZipWithSig :: Integer -> Q Dec
makeZipWithSig nSources =
    sigD
      (mkName $ "zipWith" ++ show nSources)
      (forallT typeVars
               (cxt [ classP (mkName "MonadIO") [varT (mkName "m")]
                    , classP (mkName "MonadBaseControl") [ conT (mkName "IO")
                                                        , varT (mkName "m")]])
               (zipfnsig `appT` observableSigs))
  where
    zipVars = map (mkName . (:[])) $ take (fromIntegral $ succ nSources) ['a'..]
    typeVars = map PlainTV zipVars ++ [PlainTV (mkName "m")]

    zipfnsig = arrowT `appT` zipfnsig1 zipVars
    zipfnsig1 [v] = varT v
    zipfnsig1 (v:vars) = arrowT `appT`
                         varT v `appT`
                         zipfnsig1 vars

    observableSigs = observableSigs1 zipVars
    observableSig v = (conT $ mkName "Observable") `appT`
                      (varT $ mkName "m") `appT`
                      (varT v)

    observableSigs1 [v] = observableSig v
    observableSigs1 (v:vs) = arrowT `appT`
                             observableSig v `appT`
                             observableSigs1 vs


makeZipWithFn :: Integer -> Q Dec
makeZipWithFn nSources =
  funD
    (mkName $ "zipWith" ++ show nSources)
    [ clause
        ([ varP $ mkName "zipfn" ]
         ++ map (varP . sourceName) [1 .. nSources])
        (normalB
          ([| Observable |] `appE`
            (lamE
              [varP observerName]
              (doE
                (map createQueueStmt [1 .. nSources]
                 ++ [ bindS (varP $ mkName "completedVar")
                            [| liftIO $ TVar.newTVarIO False |]
                    , bindS (varP $ mkName "completedCountVar")
                            [| liftIO  $ TVar.newTVarIO 0 |]
                    , bindS (varP $ mkName "subscriptionsVar")
                            [| liftIO $ TVar.newTVarIO [] |]
                    , letS  [
                      (valD (varP $ mkName "allQueues")
                            (normalB
                              $ tupE
                              $ map (varE . queueName) [1 .. nSources])
                            [])
                      , (valD (varP $ mkName "allSources")
                              (normalB
                                $ tupE
                                $ map (varE . sourceName) [1 .. nSources])
                              [])]
                    , noBindS (varE (mkName ("zipObserver" ++ show nSources)) `appE`
                               varE (mkName "zipfn") `appE`
                               varE (mkName "subscriptionsVar") `appE`
                               varE (mkName "completedVar") `appE`
                               varE (mkName "completedCountVar") `appE`
                               varE (mkName "allSources") `appE`
                               varE (mkName "allQueues") `appE`
                               varE (mkName "observer"))])))))
           [ makeZipObserverFn nSources
           , makeOnNextFn nSources
           , makeOnCompletedFn nSources ]]



  where
    observerName = mkName "observer"
    sourceName i = mkName $ "source" ++ show i
    queueName i = mkName $ "q" ++ show i
    createQueueStmt i = bindS (varP $ queueName i)
                              [| liftIO (TBQueue.newTBQueueIO 100) |]
