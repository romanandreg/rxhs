{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
module Rx.Observable.Distinct
       (distinct, distinctUntilChangedWith, distinctUntilChanged) where

import qualified Control.Concurrent.STM.TVar as TVar
import qualified Control.Concurrent.MVar.Lifted as MVar
import qualified Data.Set as Set
import Rx.Observable.Core (decorateOnNext)
import Rx.Types


-- | Returns an `Observable` that emits all items emitted by the source
-- `Observable` that are distinct.
--
-- <<images/distinct.png>>
--
distinct :: (Eq a, Ord a, MonadIO m, MonadBaseControl IO m)
         => Observable m a
         -> Observable m a
distinct source =
  Observable $ \observer -> do
    cacheVar <- liftIO $ MVar.newMVar Set.empty
    subscribe source
      $ Observer
      $ \case
          OnError err -> onError observer err
          OnCompleted -> onCompleted observer
          OnNext v -> do
            MVar.modifyMVar_ cacheVar $ \cache -> do
              if Set.member v cache
                then return cache
                else do
                  onNext observer v
                  return $ Set.insert v cache

-- | Returns an `Observable` that emits all items emitted by the source
-- `Observable` that are distinct from their immediate predecessors,
-- according to a key selector function.
--
-- <<images/distinctUntilChangedWith.png>>
--
distinctUntilChangedWith :: (Eq b, MonadIO m, MonadBaseControl IO m)
                         => (a -> b)
                         -> Observable m a
                         -> Observable m a
distinctUntilChangedWith transfn source =
    Observable $ \observer -> do
      priorValVar <- liftIO $ MVar.newEmptyMVar
      subscribe source
        $ Observer
        $ decorateOnNext observer
        $ onNext' priorValVar observer
  where
    onNext' priorValVar observer val = do
      mpriorVal <- liftIO $ MVar.tryTakeMVar priorValVar
      case mpriorVal of
        Nothing -> do
          liftIO $ MVar.putMVar priorValVar val
          onNext observer val
        Just priorVal
          | transfn priorVal == transfn val -> do
            liftIO $ MVar.putMVar priorValVar priorVal
            return ()
          | otherwise -> do
            liftIO $ MVar.putMVar priorValVar val
            onNext observer val


-- | Returns an `Observable` that emits all items emitted by the source
-- `Observable` that are distinct from their immediate predecessors.
--
-- <<images/distinctUntilChanged.png>>
--
distinctUntilChanged :: (Eq a, MonadIO m, MonadBaseControl IO m)
                     => Observable m a -> Observable m a
distinctUntilChanged = distinctUntilChangedWith id
