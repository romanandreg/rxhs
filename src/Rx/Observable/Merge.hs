{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Merge (merge, mergeList) where

import Control.Applicative
import Control.Monad (when, unless)
import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TVar as TVar
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)

import qualified Data.HashMap.Strict as HashMap
import qualified Data.Unique as Unique

import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import Rx.Observable.Constructors (fromList)
import qualified Rx.Observable.Map as Observable
import Rx.Observable.Core ()
import Rx.Utils (compareAndSet)
import Rx.Types


-- | Flattens an `Observable` that emits Observables into a single
--  `Observable` that emits the items emitted by those Observables,
--  without any transformation.
--
-- <<images/merge.png>>
--
-- You can combine the items emitted by multiple Observables so that
-- they appear as a single Observable, by using the merge method.
merge :: (MonadIO m, MonadBaseControl IO m)
      => (Observable m (Observable m v))
      -> Observable m v
merge sourceObservables = Observable $ \observer -> do
    completeSubscription  <- liftIO $ SAS.empty

    stoppedVar            <- liftIO $ TVar.newTVarIO False
    parentCompletedVar    <- liftIO $ TVar.newTVarIO False
    childObserversVar     <- liftIO $ TVar.newTVarIO HashMap.empty
    childSubscriptionsVar <- liftIO $ TVar.newTVarIO HashMap.empty

    let parentObserverFn = createParentObserver stoppedVar
                                                parentCompletedVar
                                                childObserversVar
                                                childSubscriptionsVar
                                                observer

    subscription <- subscribe sourceObservables parentObserverFn
    liftIO $ SAS.set subscription completeSubscription

    return $ toSubscription completeSubscription
  where
    createParentObserver :: forall m v . (MonadIO m, MonadBaseControl IO m)
                         => TVar.TVar Bool
                         -> TVar.TVar Bool
                         -> TVar.TVar (HashMap.HashMap Int (Observer m v))
                         -> TVar.TVar (HashMap.HashMap Int Subscription)
                         -> Observer m v
                         -> Observer m (Observable m v)
    createParentObserver stoppedVar
                         parentCompletedVar
                         childObserversVar
                         childSubscriptionsVar
                         observer =
        Observer parentObserverFn
      where
        stopSubscriptions = do
          stopped <- compareAndSet stoppedVar False True
          if stopped
            then do
              subscriptions <- STM.atomically $ TVar.readTVar childSubscriptionsVar
              mapM_ unsubscribe $ HashMap.elems subscriptions
              return True
            else return False

        parentObserverFn (OnError err) = onError observer err

        parentObserverFn OnCompleted = do
          liftIO $ STM.atomically $ TVar.writeTVar parentCompletedVar True

          childCount <- liftIO $ HashMap.size <$> (STM.atomically $
                                                   TVar.readTVar childObserversVar)

          when (childCount == 0) $ do
            isStopped <- liftIO $ STM.atomically $ TVar.readTVar stoppedVar
            unless isStopped $ do
              wasStopped <- liftIO stopSubscriptions
              when wasStopped $ do
                onCompleted observer

        parentObserverFn (OnNext childObservable) = do
          isStopped <- liftIO $ STM.atomically $ TVar.readTVar stoppedVar
          unless isStopped $ do
            observerId <- liftIO $ Unique.hashUnique <$> Unique.newUnique

            let childObserver = Observer (childObserverFn observerId)

            liftIO $ STM.atomically
                   $ TVar.modifyTVar childObserversVar
                                     (HashMap.insert observerId childObserver)

            subscription <- subscribe childObservable childObserver

            liftIO $ STM.atomically
                   $ TVar.modifyTVar childSubscriptionsVar
                                     (HashMap.insert observerId subscription)


        childObserverFn observerId OnCompleted = do
          liftIO $ STM.atomically
                 $ TVar.modifyTVar childObserversVar
                                   (HashMap.delete observerId)

          isStopped <- liftIO $ STM.atomically $ TVar.readTVar stoppedVar
          unless isStopped $ do
            childCount <- liftIO $ HashMap.size <$> (STM.atomically $
                                                     TVar.readTVar childObserversVar)
            completed <- liftIO $ STM.atomically
                                $ TVar.readTVar parentCompletedVar

            when (childCount == 0 && completed) $ do
              wasStopped <- liftIO stopSubscriptions
              when wasStopped $ onCompleted observer

        childObserverFn _ (OnError err) = do
          isStopped <- liftIO $ STM.atomically $ TVar.readTVar stoppedVar
          unless isStopped $ do
            wasStopped <- liftIO stopSubscriptions
            when wasStopped $ onError observer err

        childObserverFn _ (OnNext val) = do
          isStopped <- liftIO $ STM.atomically $ TVar.readTVar stoppedVar
          unless isStopped $ do
            onNext observer val

-- | Flattens a List of Observables into one `Observable`, without
-- any transformation.
--
-- <<images/mergeList.png>>
--
--  You can combine items emitted by multiple Observables so that they
--  appear as a single Observable, by using the merge method.
--
mergeList :: (MonadIO m, MonadBaseControl IO m)
          => [Observable m v]
          -> Observable m v
mergeList observables = merge (fromList observables)
