{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.TakeUntil (takeUntil) where

import Data.Maybe (fromJust, isJust)
import qualified Rx.Observable.Map as Observable
import qualified Rx.Observable.Merge as Observable
import qualified Rx.Observable.TakeWhile as Observable
import Rx.Observable.Core ()
import Rx.Types

takeUntil :: ( MonadIO m, MonadBaseControl IO m )
           => Observable m a
           -> Observable m b
           -> Observable m a
takeUntil source other =
  Observable $ \observer ->
    flip subscribe observer
      $ Observable.map fromJust
      $ Observable.takeWhile isJust
      $ Observable.mergeList [ Observable.map Just source
                             , Observable.map (const $ Nothing) other ]
