{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Core where

import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)
import qualified Rx.Observer as Observer
import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import Rx.Types

--------------------------------------------------------------------------------

decorateOnNext :: IObserver m o
               => o v -> (t -> m ()) -> Notification t -> m ()
decorateOnNext observer onNextFn = newObserver
  where
    newObserver (OnNext v) = onNextFn v
    newObserver (OnError err) = onError observer err
    newObserver OnCompleted = onCompleted observer

--------------------------------------------------------------------------------

instance (MonadIO m, MonadBaseControl IO m) => IObservable m (Observable m) where
  subscribe source ob = do
    outerSub <- liftIO $ SAS.empty
    safeOb   <- Observer.safe outerSub ob
    innerSub <- observableOnSubscribe source safeOb
    liftIO $ SAS.set innerSub outerSub
    return $ toSubscription outerSub

--------------------------------------------------------------------------------

instance (MonadIO m, MonadBaseControl IO m) =>
         IObservable m (ConnectableObservable m) where
  subscribe (ConnectableObservable isConnectedVar
                                   subject sourceSub
                                   connectedBufferVar
                                   replayerVar)
            observer = do

    cs        <- liftIO CS.create
    observer' <- Observer.safe (toSubscription cs) observer
    subjSub   <- subscribe subject observer'


    replayer <- liftIO . atomically $ TVar.readTVar replayerVar
    _replayerReplayAction replayer

    liftIO $ do
      CS.append subjSub cs
      CS.append sourceSub cs

    return $ toSubscription cs

--------------------------------------------------------------------------------

instance ToObservable Subject where
  toObservable = Observable . _subjectOnSubscribe

instance (MonadIO m, MonadBaseControl IO m) => IObservable m (Subject m) where
  subscribe = subscribe . toObservable

instance (MonadIO m, MonadBaseControl IO m) => IObserver m (Subject m) where
  emitNotification = _subjectOnEmitNotification

--------------------------------------------------------------------------------

instance (MonadIO m, MonadBaseControl IO m) => IObservable m (GenericObservable m) where
  subscribe (GenericObservable source) = subscribe source
