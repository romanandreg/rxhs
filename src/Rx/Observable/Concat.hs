{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Concat (concat, concatList) where

import Prelude hiding (concat)

import Control.Monad (void, when)

import qualified Control.Concurrent.MVar.Lifted as MVar
import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar
import qualified Control.Concurrent.STM.TQueue as TQueue


import qualified Rx.Subscription.Subscription as Subscription
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS

import qualified Rx.Scheduler as Scheduler

import Rx.Observable.Core ()
import Rx.Observable.Constructors (fromList)
import qualified Rx.Observable.Map as Observable
import Rx.Utils (compareAndSet)
import Rx.Types


genericConcat :: (MonadIO m, MonadBaseControl IO m)
              => Observable m (GenericObservable m v)
              -> Observable m v
genericConcat sequences =
    Observable $ \observer -> do
      completedOrErred  <- liftIO $ TVar.newTVarIO False
      allSequencesReceived <- liftIO $ TVar.newTVarIO False
      outerSubscription <- liftIO $ SAS.empty
      innerSubscriptionVar <- liftIO $ MVar.newMVar Nothing
      queue <- liftIO $ TQueue.newTQueueIO
      main observer
           allSequencesReceived completedOrErred
           outerSubscription innerSubscriptionVar
           queue

      sub <- liftIO $ Subscription.create $ do
        unsubscribe $ toSubscription outerSubscription
        msubscription <- liftIO $ MVar.readMVar innerSubscriptionVar
        maybe (return ()) unsubscribe msubscription
      return sub
  where
    main observer
         allSequencesReceived completedOrErred
         outerSubscription innerSubscriptionVar
         queue = do
        sub <- subscribe sequences $ Observer outerObserver
        liftIO $ SAS.set sub outerSubscription
        return $ toSubscription outerSubscription
      where
        outerObserver (OnNext source) =
          MVar.modifyMVar_ innerSubscriptionVar $ \msubscription -> do
            case msubscription of
              Just _ -> do
                liftIO . atomically $ TQueue.writeTQueue queue source
                return msubscription
              Nothing -> do
                sub <- subscribe source $ Observer innerObserver
                return $ Just sub

        outerObserver (OnError err) = do
          liftIO $ do
            result <- compareAndSet completedOrErred False True
            when result $ do
              msubscription <- MVar.readMVar innerSubscriptionVar
              maybe (return ()) unsubscribe msubscription
          onError observer err

        outerObserver OnCompleted = do
          liftIO . atomically $ TVar.writeTVar allSequencesReceived True
          msubscription <- MVar.readMVar innerSubscriptionVar
          case msubscription of
            Nothing -> do
              completed <- liftIO $ compareAndSet completedOrErred False True
              when completed $ onCompleted observer
            _ -> return ()

        innerObserver (OnNext v) = onNext observer v

        innerObserver (OnError err) = do
          completed <- liftIO $ compareAndSet completedOrErred False True
          when completed $ do
            liftIO $ unsubscribe $ toSubscription outerSubscription
            onError observer err

        innerObserver OnCompleted = do
          isEmpty <- liftIO . atomically $ TQueue.isEmptyTQueue queue
          allReceived <- liftIO . atomically $ TVar.readTVar allSequencesReceived
          if (isEmpty && allReceived)
             then do
               completed <- liftIO $ compareAndSet completedOrErred False True
               when completed $ onCompleted observer

             else do
               source <- liftIO . atomically $ TQueue.readTQueue queue
               sub    <- subscribe source $ Observer innerObserver
               void $ MVar.swapMVar innerSubscriptionVar (Just sub)

-- | Returns an `Observable` that emits the items emitted by each of the
-- Observables emitted by an `Observable`, one after the other, without
-- interleaving them.
--
-- <<images/concat.png>>
--
concat :: (IObservable m o, MonadIO m, MonadBaseControl IO m)
       => Observable m (o v)
       -> Observable m v
concat = genericConcat . Observable.map GenericObservable

-- | Returns an `Observable` that emits the items emitted by each of
-- the Observables in the input list, one after the other, without
-- interleaving them.
--
concatList :: (IObservable m o, MonadIO m, MonadBaseControl IO m)
           => [o v]
           -> Observable m v
concatList = concat . fromList
