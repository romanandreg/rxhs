{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Timer where


import Control.Monad (void)
import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import qualified Rx.Scheduler as Scheduler
import Rx.Observable.Core ()
import Rx.Types


timerWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                   => Scheduler m
                   -> TimeInterval
                   -> Observable m Int
timerWithScheduler scheduler delay =
  Observable $ \observer -> do
    Scheduler.scheduleTimed scheduler delay $ do
      onNext observer 0
      onCompleted observer

timer :: (MonadIO m, MonadBaseControl IO m)
      => TimeInterval
      -> Observable m Int
timer = timerWithScheduler Scheduler.newThreadScheduler

--------------------------------------------------------------------------------

intervalWithScheduler :: (Enum a, Num a, MonadIO m, MonadBaseControl IO m)
                      => Scheduler m
                      -> TimeInterval
                      -> TimeInterval
                      -> Observable m a
intervalWithScheduler scheduler initialDelay period =
  Observable $ \observer -> do
    cs  <- liftIO CS.create
    sas <- liftIO SAS.empty
    outerSub <- Scheduler.scheduleTimed scheduler initialDelay $ do
      innerSub <- Scheduler.scheduleTimedRecursiveState scheduler 0 period
                    $ \i recur -> do onNext observer i
                                     recur (succ i)
      liftIO $ SAS.set innerSub sas

    liftIO $ CS.append outerSub cs
    liftIO $ CS.append (toSubscription sas) cs
    return $ toSubscription cs

interval :: (Enum a, Num a, MonadIO m, MonadBaseControl IO m)
         => TimeInterval
         -> TimeInterval
         -> Observable m a
interval = intervalWithScheduler Scheduler.newThreadScheduler
