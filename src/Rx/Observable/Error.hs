{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Error where

import Control.Exception (SomeException)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)
import qualified Rx.Subscription.BooleanSubscription as BS
import qualified Rx.Subscription.CompositeSubscription as CS
import Rx.Observable.Core ()
import Rx.Types

-- | Instruct an `Observable` to emit an item (returned by a specified
-- function) rather than invoking onError if it encounters an error.
--
-- <<images/retry.png>>
--
onErrorReturn :: (MonadIO m, MonadBaseControl IO m)
              => a
              -> Observable m a
              -> Observable m a
onErrorReturn errorVal source =
    Observable $ \observer -> do
      subscribe source $ Observer (observerFn observer)
  where
    observerFn observer (OnNext val) = onNext observer val
    observerFn observer OnCompleted  = onCompleted observer
    observerFn observer (OnError _)  = do
      onNext observer errorVal
      onCompleted observer

-- | Instruct an `Observable` to pass control to another `Observable`
-- rather than invoking onError if it encounters an Error.
--
-- <<images/onErrorResumeNext.png>>
--
onErrorResumeNext :: (MonadIO m, MonadBaseControl IO m)
                      => Observable m a -> Observable m a -> Observable m a
onErrorResumeNext fallbackObs source =
    Observable $ \observer -> do
      bs <- liftIO $ BS.empty
      sub <- subscribe source $ Observer (observerFn bs observer)
      liftIO $ BS.set sub bs
      return $ toSubscription bs
  where
    observerFn _ observer (OnNext val) = onNext observer val
    observerFn _ observer OnCompleted  = onCompleted observer
    observerFn bs observer (OnError _)  = do
      sub2 <- subscribe fallbackObs observer
      liftIO $ BS.set sub2 bs

-- | Return an `Observable` that mirrors the source `Observable`,
-- resubscribing to it if it calls onError (infinite retry count).
--
-- <<>>
--
-- If the source `Observable` calls `Rx.Observer.onError`, this method
-- will resubscribe to the source `Observable` rather than propagating
-- the onError call.
--
retry :: (MonadIO m, MonadBaseControl IO m)
      => Observable m a -> Observable m a
retry source =
    Observable $ \observer -> do
      bs <- liftIO $ BS.empty
      sub1 <- subscribe source $ Observer (observerFn bs observer)
      liftIO $ BS.set sub1 bs
      return $ toSubscription bs
  where
    observerFn _ observer (OnNext val) = onNext observer val
    observerFn _ observer OnCompleted  = onCompleted observer
    observerFn bs observer (OnError _) = do
      sub <- subscribe source $ Observer (observerFn bs observer)
      liftIO $ BS.set sub bs

-- | Return an `Observable` that mirrors the source `Observable`,
-- resubscribing to it if it calls `onError` up to a specified number of
-- retries.
--
-- <<images/retry.png>>
--
retryCount :: (MonadIO m, MonadBaseControl IO m)
      => Int -> Observable m a -> Observable m a
retryCount maxCount source = do
    Observable $ \observer -> do
      bs <- liftIO $ BS.empty
      sub1 <- subscribe source $ Observer (observerFn maxCount bs observer)

      liftIO $ BS.set sub1 bs
      return $ toSubscription bs
  where
    observerFn _ _ observer (OnNext val) = onNext observer val
    observerFn _ _ observer OnCompleted = onCompleted observer
    observerFn 0 bs observer (OnError err) = onError observer err
    observerFn n bs observer _ = do
      sub <- subscribe source $ Observer (observerFn (n - 1) bs observer)
      liftIO $ BS.set sub bs

catch :: (MonadIO m, MonadBaseControl IO m)
      => (SomeException -> m ())
      -> Observable m a
      -> Observable m a
catch errHandler source =
  Observable $ \observer ->
    subscribe source
      $ Observer
      $ \case
          OnNext v -> onNext observer v
          OnError err ->
            errHandler err >> onCompleted observer
          OnCompleted -> onCompleted observer
