{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Time where

import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar
import Control.Monad (unless)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)
import Data.Time.Clock (NominalDiffTime, UTCTime, addUTCTime, diffUTCTime)
import Rx.Observable.Core (decorateOnNext)
import qualified Rx.Scheduler as Scheduler
import Rx.Types

--------------------------------------------------------------------------------

timestampWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                       => Scheduler m
                       -> Observable m a
                       -> Observable m (UTCTime, a)
timestampWithScheduler scheduler source =
  Observable $ \observer -> do
    subscribe source
      $ Observer
      $ decorateOnNext observer
      $ \v -> do
          timestamp <- Scheduler.now scheduler
          onNext observer (timestamp, v)

timestamp :: (MonadIO m, MonadBaseControl IO m)
          => Observable m a -> Observable m (UTCTime, a)
timestamp =
  timestampWithScheduler Scheduler.immediateScheduler

--------------------------------------------------------------------------------

timeIntervalWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                          => Scheduler m
                          -> Observable m a
                          -> Observable m NominalDiffTime
timeIntervalWithScheduler scheduler source =
  Observable $ \observer -> do
    initialTime <- Scheduler.now scheduler
    timestampVar <- liftIO $ TVar.newTVarIO initialTime
    subscribe source
      $ Observer
      $ decorateOnNext observer
      $ \v -> do
        currentTime <- Scheduler.now scheduler
        interval <- liftIO $ do
          prevTime <- atomically $ do
            prevTime <- TVar.readTVar timestampVar
            TVar.writeTVar timestampVar currentTime
            return prevTime
          return  $ diffUTCTime currentTime prevTime
        onNext observer interval

timeInterval :: (MonadIO m, MonadBaseControl IO m)
             => Observable m a -> Observable m NominalDiffTime
timeInterval =
  timeIntervalWithScheduler Scheduler.immediateScheduler

--------------------------------------------------------------------------------

delaySubscriptionWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                               => Scheduler m -> TimeInterval -> Observable m a
                               -> Observable m a
delaySubscriptionWithScheduler scheduler interval source =
  Observable $ \observer -> do
    initialTime <- Scheduler.now scheduler
    let delaySeconds = toNominalDiffTime interval
        timeAfterDelay = addUTCTime delaySeconds initialTime
    subscribe source
      $ Observer
      $ decorateOnNext observer
      $ \v -> do
          currentTime <- Scheduler.now scheduler
          unless (currentTime < timeAfterDelay) $ onNext observer v

delaySubscription :: (MonadIO m, MonadBaseControl IO m)
                  => TimeInterval -> Observable m a
                  -> Observable m a
delaySubscription =
  delaySubscriptionWithScheduler Scheduler.immediateScheduler
