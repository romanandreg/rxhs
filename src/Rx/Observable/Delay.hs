{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Delay where

import qualified Control.Concurrent.STM.TVar as TVar
import qualified Data.HashMap.Strict as HashMap
import qualified Data.Unique as Unique

import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subscription.Subscription as Subscription

import qualified Rx.Scheduler as Scheduler
import qualified Rx.Observable.Concat as Observable
import qualified Rx.Observable.Map as Observable
import qualified Rx.Observable.Timer as Observable
import qualified Rx.Observable.Replay as Observable
import qualified Rx.Observable.Connectable as Observable
import Rx.Observable.Core ()
import Rx.Types


-- | Returns an `Observable` that emits the items emitted by the source
-- `Observable` shifted forward in time by a specified delay. Error
-- notifications from the source `Observable` are not delayed.
--
-- <<images/delayWithScheduler.png>>
--
delayWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                   => Scheduler m
                   -> TimeInterval
                   -> Observable m a
                   -> Observable m a
delayWithScheduler scheduler delayTime =
    Observable.concat . Observable.mapM delaySingle
  where
    delaySingle item = do
      let ob = Observable.timerWithScheduler scheduler delayTime
      obs <- Observable.replay $ Observable.map (const item) ob
      Observable.connect obs
      return obs

-- | Returns an `Observable` that emits the items emitted by the source
-- `Observable` shifted forward in time by a specified delay. Error
-- notifications from the source `Observable` are not delayed.
--
-- <<images/delay.png>>
--
delay :: (MonadIO m, MonadBaseControl IO m)
      => TimeInterval -> Observable m a -> Observable m a
delay = delayWithScheduler Scheduler.newThreadScheduler
