{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Amb (amb) where

import Control.Applicative ((<$>))
import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar

import qualified Data.HashMap.Strict as HashMap
import qualified Data.Unique as Unique

import Control.Monad (forM, forM_)
import qualified Rx.Subscription.Subscription as Subscription
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import Rx.Observable.Core ()
import Rx.Types


genericAmb :: (MonadIO m, MonadBaseControl IO m)
           => [GenericObservable m a]
           -> Observable m a
genericAmb sequences =
    Observable $ \observer -> do
      selectedSubscription <- liftIO $ TVar.newTVarIO (Left HashMap.empty)
      main observer selectedSubscription
  where
    main observer selectedSubscription = do
        pairs <- forM sequences $ \observable -> do
          sub      <- liftIO $ SAS.empty
          subId    <- liftIO $ Unique.hashUnique <$> Unique.newUnique
          innerSub <- subscribe observable $ Observer (observerFn subId)
          liftIO $ SAS.set innerSub sub
          return (subId, sub)

        let subscriptions = HashMap.fromList pairs
        liftIO . atomically
          $ TVar.writeTVar selectedSubscription (Left subscriptions)

        ambSubscription <- liftIO $ Subscription.create $ do
          result <- atomically $ TVar.readTVar selectedSubscription
          case result of
            Left allSubscriptions ->
              mapM_ unsubscribe $ HashMap.elems allSubscriptions
            Right subscription -> do
              unsubscribe subscription

        return ambSubscription
      where
        observerFn subId (OnNext v) = do
          result <- liftIO . atomically $ TVar.readTVar selectedSubscription
          case result of
            Left allSubscriptions ->
              liftIO $ do
                forM_ (HashMap.toList allSubscriptions)
                  $ \(subId', sub) -> do
                    if (subId' == subId)
                       then liftIO . atomically
                              $ TVar.writeTVar selectedSubscription (Right sub)
                       else liftIO $ unsubscribe sub
            _ -> return ()

          onNext observer v

        observerFn _ (OnError err) = onError observer err
        observerFn _ OnCompleted = onCompleted observer

-- | Given two or more source Observables, emits all of the items from
-- the first of these Observables to emit an item.
--
-- <<images/amb.png>>
--
-- When you pass a number of source Observables to @amb@, it will
-- pass through the emissions and messages of exactly one of these
-- Observables: the first one that emits an item to @amb@. It will
-- ignore and discard the emissions of all of the other source
-- Observables.
--
amb :: (MonadIO m, MonadBaseControl IO m, IObservable m o)
    => [o v]
    -> Observable m v
amb = genericAmb . map GenericObservable
