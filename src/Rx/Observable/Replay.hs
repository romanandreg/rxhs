{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Replay (
    replay
  , replayWithScheduler
  , replayBuffer
  , replayBufferWithScheduler
  , replayIntervalAndBuffer
  , replayIntervalAndBufferWithScheduler
  ) where

import Control.Applicative ((<$>))
import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar

import Control.Monad (void, when)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl(..))

import Data.Time.Clock (diffUTCTime)
import qualified Data.Sequence as Seq
import qualified Data.Foldable as Seq (toList)

import qualified Rx.Scheduler as Scheduler
import Rx.Subject.PublishSubject ()
import Rx.Observable.Core ()
import Rx.Observable.Connectable

import Rx.Types

-------------------------------------------------------------------------------

replayEach :: (Functor m, MonadIO m, IObserver m o)
           => Scheduler m -> o v -> TVar.TVar Bool
           -> TVar.TVar (Seq.Seq (Notification v))
           -> m ()
replayEach scheduler subject isConnectedVar bufferVar = do
  (ns, isConnected) <- liftIO . atomically $ do
    ns <- TVar.readTVar bufferVar
    isConnected <- TVar.readTVar isConnectedVar
    return (ns, isConnected)

  when isConnected $ do
    void . Scheduler.schedule scheduler $ do
      mapM_ (emitNotification subject) $ Seq.toList ns

-------------------------------------------------------------------------------

-- | Returns a `ConnectableObservable` that shares a single subscription
-- to the underlying `Observable` that will replay all of its items and
-- notifications to any future `Observer`.
--
-- <<images/replay.png>>
--
replayWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                    => Scheduler m
                    -> Observable m a
                    -> m (ConnectableObservable m a)
replayWithScheduler scheduler source0 = do
    source@(ConnectableObservable isConnectedVar subject _ _ replayerVar) <-
      multicast source0
    bufferVar <- liftIO $ TVar.newTVarIO Seq.empty
    let replayer = Replayer (replayAdd bufferVar)
                            (replayEach scheduler subject isConnectedVar bufferVar)
    liftIO . atomically $ TVar.writeTVar replayerVar replayer
    return source
  where
    replayAdd bufferVar notification = do
      liftIO . atomically $
        TVar.modifyTVar bufferVar (Seq.|> notification)


-- | Same as `replayWithScheduler` with a default `immediateScheduler`
--
replay :: (MonadIO m, MonadBaseControl IO m)
       => Observable m a
       -> m (ConnectableObservable m a)
replay = replayWithScheduler Scheduler.immediateScheduler

--------------------------------------------------------------------------------

-- | Returns a `ConnectableObservable` that shares a single subscription
-- to the source `Observable` that replays at most `bufferSize` items
-- emitted by that `Observable`.
--
--  <<images/replayCount.png>>
--
replayBufferWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                         => Scheduler m
                         -> Int
                         -- ^ the buffer size that limits the number
                         -- of items that can be replayed
                         -> Observable m a
                         -- ^ source Observable
                         -> m (ConnectableObservable m a)
replayBufferWithScheduler scheduler bufferSize source0  = do
    source@(ConnectableObservable isConnectedVar subject _ _ replayerVar)
      <- multicast source0
    bufferVar <- liftIO $ TVar.newTVarIO Seq.empty
    let replayer = Replayer (replayAdd bufferVar)
                            (replayEach scheduler subject isConnectedVar bufferVar)
    liftIO . atomically $ TVar.writeTVar replayerVar replayer
    return source
  where
    replayAdd bufferVar notification =
      liftIO . atomically $ do
        count <- Seq.length <$> TVar.readTVar bufferVar
        when (count < bufferSize) $
          TVar.modifyTVar bufferVar (Seq.|> notification)


-- | Same as replayCountWithScheduler with a default `immediateScheduler`
--
replayBuffer
  :: (MonadIO m, MonadBaseControl IO m)
  => Int            -- ^ buffer size
  -> Observable m a -- ^ source Observable
  -> m (ConnectableObservable m a)
replayBuffer =
  replayBufferWithScheduler Scheduler.immediateScheduler

--------------------------------------------------------------------------------

replayIntervalWithScheduler
  :: (MonadIO m, MonadBaseControl IO m)
  => Scheduler m
  -> TimeInterval
  -> Observable m a
  -> m (ConnectableObservable m a)
replayIntervalWithScheduler scheduler delay source0 = do
    source@(ConnectableObservable isConnectedVar subject _ _ replayerVar)
      <- multicast source0

    startTime <- Scheduler.now scheduler
    bufferVar <- liftIO $ TVar.newTVarIO Seq.empty
    let replayer = Replayer (replayAdd startTime bufferVar)
                            (replayEach scheduler subject isConnectedVar bufferVar)
    liftIO . atomically $ TVar.writeTVar replayerVar replayer
    return source
  where
    replayAdd startTime bufferVar notification = do
      currentTime <- Scheduler.now scheduler
      when (diffUTCTime startTime currentTime < toNominalDiffTime delay)
        $ liftIO . atomically
        $ TVar.modifyTVar bufferVar (Seq.|> notification)


replayInterval
  :: (MonadIO m, MonadBaseControl IO m)
  => TimeInterval
  -> Observable m a
  -> m (ConnectableObservable m a)
replayInterval =
  replayIntervalWithScheduler Scheduler.immediateScheduler

--------------------------------------------------------------------------------

replayIntervalAndBufferWithScheduler
  :: (MonadIO m, MonadBaseControl IO m)
  => Scheduler m
  -> TimeInterval
  -> Int
  -> ConnectableObservable m a
  -> m ()
replayIntervalAndBufferWithScheduler scheduler delay bufferSize
  (ConnectableObservable isConnectedVar subject _ _ replayerVar) = do
    startTime <- Scheduler.now scheduler
    bufferVar <- liftIO $ TVar.newTVarIO Seq.empty
    let replayer = Replayer (replayAdd startTime bufferVar)
                            (replayEach scheduler subject isConnectedVar bufferVar)
    liftIO . atomically $ TVar.writeTVar replayerVar replayer
  where
    replayAdd startTime bufferVar notification = do
      currentTime <- Scheduler.now scheduler
      when (diffUTCTime startTime currentTime < toNominalDiffTime delay)
        $ liftIO . atomically
        $ do
          count <- Seq.length <$> TVar.readTVar bufferVar
          when (count < bufferSize) $
            TVar.modifyTVar bufferVar (Seq.|> notification)

replayIntervalAndBuffer
  :: (MonadIO m, MonadBaseControl IO m)
  => TimeInterval
  -> Int
  -> ConnectableObservable m a
  -> m ()
replayIntervalAndBuffer =
  replayIntervalAndBufferWithScheduler Scheduler.immediateScheduler
