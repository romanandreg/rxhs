{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Zip
       ( zipWith
       , zipWith3
       , zipWith4
       , zipWith5
       , zipWith6
       , zipWith7
       , zipWith8
       , zip
       ) where

import Language.Haskell.TH (mkName)

import Prelude hiding (zipWith, zipWith3, zip)

import Control.Monad (unless, when)
import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TBQueue as TBQueue
import qualified Control.Concurrent.STM.TVar as TVar
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)
import Rx.Observer ()
import Rx.Observable.Core ()
import qualified Rx.Subscription.CompositeSubscription as CS
import Rx.Utils (compareAndSet)
import Rx.Observable.TH.Zip
import Rx.Types


$(makeZipWith 2)
$(makeZipWith 3)
$(makeZipWith 4)
$(makeZipWith 5)
$(makeZipWith 6)
$(makeZipWith 7)
$(makeZipWith 8)

zipWith :: (MonadIO m, MonadBaseControl IO m)
        => (a -> b -> c)
        -> Observable m a -> Observable m b -> Observable m c
zipWith = zipWith2

zip :: (MonadIO m, MonadBaseControl IO m)
    => Observable m a -> Observable m b -> Observable m (a, b)
zip = zipWith (,)
