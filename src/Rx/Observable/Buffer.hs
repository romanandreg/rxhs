{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
module Rx.Observable.Buffer where

import qualified Control.Concurrent.STM.TVar as TVar
import qualified Data.Foldable as Seq (toList)
import qualified Data.Sequence as Seq
import Data.Maybe (isJust)
import Data.Time.Clock (diffUTCTime)
import qualified Rx.Scheduler as Scheduler
import Rx.Observer ()
import Rx.Observable.Core ()
import qualified Rx.Subscription.CompositeSubscription as CS
import Rx.Types

-- | Returns an `Observable` that emits buffers of items it collects
-- from the source `Observable`. The resulting `Observable` emits buffers
-- every skip items, each containing count items. When the source
-- `Observable` completes or encounters an error, the resulting
-- `Observable` emits the current buffer and propagates the notification
-- from the source `Observable`.
--
-- <<images/bufferSkipAndCount.png>>
--
bufferSkipAndCount :: (MonadIO m, MonadBaseControl IO m)
                       => Int -> Int -> Observable m a -> Observable m [a]
bufferSkipAndCount skipSize bufferSize source =
    Observable $ \observer -> do
      bufferVar <- liftIO $ TVar.newTVarIO Seq.empty
      subscribe source $ Observer (observerFn bufferVar observer)
  where
    sendPending bufferVar observer = do
      buffer <- liftIO $ atomically $ TVar.readTVar bufferVar
      unless (Seq.null buffer) $
        onNext observer (Seq.toList buffer)
    observerFn bufferVar observer (OnNext v) = do
      liftIO $ atomically $ TVar.modifyTVar bufferVar (Seq.|> v)
      buffer <- liftIO $ atomically $ TVar.readTVar bufferVar
      let size = Seq.length buffer
      when (size == skipSize) $ do
        liftIO $ atomically $ TVar.writeTVar bufferVar Seq.empty
        onNext observer (Seq.toList $ Seq.take bufferSize buffer)
    observerFn bufferVar observer (OnError err) = do
      sendPending bufferVar observer
      onError observer err
    observerFn bufferVar observer OnCompleted = do
      sendPending bufferVar observer
      onCompleted observer

-- | Returns an `Observable` that emits buffers of items it collects
-- from the source `Observable`. The resulting `Observable` emits
-- connected, non-overlapping buffers, each containing count
-- items. When the source Observable completes or encounters an error,
-- the resulting `Observable` emits the current buffer and propagates
-- the notification from the source `Observable`.
--
-- <<images/bufferCount.png>>
--
bufferCount :: (MonadIO m, MonadBaseControl IO m)
                => Int -> Observable m a -> Observable m [a]
bufferCount bufferSize =
  bufferSkipAndCount bufferSize bufferSize

-- | Monitors an `Observable` and starts a buffering of values when
-- bufferOpenings returns True, it will stop the buffering when the
-- closingSelector function returns True.
--
-- <<images/bufferConditions.png>>
--
bufferConditions
  :: (MonadIO m, MonadBaseControl IO m)
  => (a -> m Bool)     -- ^ bufferOpenings
  -> (a -> m Bool)     -- ^ closingSelector
  -> Observable m a
  -> Observable m [a]
bufferConditions bufferOpenings closingSelector source = do
    Observable $ \observer -> do
      bufferVar <- liftIO $ TVar.newTVarIO Nothing
      subscribe source
        $ Observer
        $ \case
            OnNext v -> do
              isBuffering <- onBuffering bufferVar
              if isBuffering
                 then do
                   liftIO $ atomically $ TVar.modifyTVar bufferVar (fmap (Seq.|> v))
                   shouldEnd <- closingSelector v
                   when shouldEnd $ sendPending bufferVar observer
                 else do
                   shouldStart <- bufferOpenings v
                   when shouldStart $
                     liftIO $ atomically
                            $ TVar.writeTVar bufferVar (Just $ Seq.singleton v)

            OnError err -> do
              sendPending bufferVar observer
              onError observer err

            OnCompleted -> do
              sendPending bufferVar observer
              onCompleted observer
  where
    sendPending bufferVar observer = do
      mbuffer <- liftIO $ atomically $ TVar.readTVar bufferVar
      case mbuffer of
        Just buffer -> do
          onNext observer (Seq.toList buffer)
          liftIO $ atomically $ TVar.writeTVar bufferVar Nothing
        Nothing -> return ()

    onBuffering bufferVar =
      liftIO $ atomically
             $ isJust <$> TVar.readTVar bufferVar


-- | Emits a new bundle of items periodically, every timespan amount
-- of time, containing all items emitted by the source @Observable@
-- since the previous bundle emission.
--
-- <<images/bufferInterval.png>>
--
bufferIntervalWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                            => Scheduler m
                            -> TimeInterval -> Observable m a -> Observable m [a]
bufferIntervalWithScheduler scheduler interval source =
  Observable $ \observer -> do
    accVar       <- liftIO $ TVar.newTVarIO Seq.empty

    cs <- liftIO $ CS.create
    schedulerSub <- Scheduler.scheduleTimedRecursive scheduler interval $ \recur -> do
      buffer <- liftIO . atomically $ do
        buffer <- TVar.readTVar accVar
        TVar.writeTVar accVar Seq.empty
        return buffer

      onNext observer $ Seq.toList buffer
      recur

    sub <- subscribe source
      $ Observer
      $ \case
        OnNext v -> liftIO . atomically $ TVar.modifyTVar accVar (Seq.|> v)
        OnError err -> do
          liftIO $ unsubscribe schedulerSub
          onError observer err
        OnCompleted -> do
          liftIO $ unsubscribe schedulerSub
          onCompleted observer

    liftIO $ CS.append schedulerSub cs
    liftIO $ CS.append sub cs

    return $ toSubscription cs

-- | Same as `bufferIntervalWithScheduler` but using the the
-- `newThreadScheduler` as a default.
--
bufferInterval :: (MonadIO m, MonadBaseControl IO m)
               => TimeInterval
               -> Observable m a
               -> Observable m [a]
bufferInterval =
  bufferIntervalWithScheduler Scheduler.newThreadScheduler
