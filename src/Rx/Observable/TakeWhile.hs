{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.TakeWhile (takeWhile) where

import Prelude hiding (takeWhile)
import Control.Exception.Lifted (SomeException, catch)

import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import Rx.Observable.Core ()
import Rx.Types

takeWhile :: (MonadIO m, MonadBaseControl IO m)
          => (v -> Bool)
          -> Observable m v
          -> Observable m v
takeWhile predfn source =
  Observable $ \observer -> do
    sas <- liftIO SAS.empty
    sub <- subscribe source
             $ Observer
             $ \case
                 OnNext v
                   | predfn v -> onNext observer v
                   | otherwise -> do
                     onCompleted observer
                     liftIO . unsubscribe $ toSubscription sas
                 OnError err -> do
                   onError observer err
                 OnCompleted -> do
                   onCompleted observer
    liftIO $ SAS.set sub sas
    return $ toSubscription sas
