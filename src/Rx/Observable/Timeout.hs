{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Timeout where

import Data.Typeable (cast)

import Control.Exception (SomeException(..))
import qualified Control.Concurrent.STM.TVar as TVar
import Control.Monad (when)

import Rx.Observable.Core ()
import qualified Rx.Scheduler as Scheduler
import qualified Rx.Subscription.BooleanSubscription as BS
import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import Rx.Types

timeoutSelectorWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                             => Scheduler m
                             -> (v -> m Bool)
                             -> TimeInterval
                             -> Observable m v
                             -> Observable m v
timeoutSelectorWithScheduler scheduler predfn delay source =
    Observable $ \observer -> do
      allSub <- liftIO CS.create
      timeoutSub <- liftIO BS.empty
      innerObserverSub <- liftIO SAS.empty
      main allSub timeoutSub innerObserverSub observer
  where
    main allSub timeoutSub innerObserverSub observer = do
        subscription <- subscribe source $ Observer observerFn
        resetTimeout
        liftIO $ do
          SAS.set subscription innerObserverSub
          CS.append (toSubscription timeoutSub) allSub
          CS.append (toSubscription innerObserverSub) allSub

        return $ toSubscription allSub
      where
        resetTimeout = do
          subscription <- Scheduler.scheduleTimed scheduler delay $ do
            liftIO $ unsubscribe innerObserverSub
            let err = SomeException $ TimeoutException delay
            onError observer err
          liftIO $ BS.set subscription timeoutSub

        observerFn (OnNext v) = do
          shouldResetTimeout <- predfn v
          when shouldResetTimeout resetTimeout
          onNext observer v

        observerFn notification = do
          liftIO $ unsubscribe timeoutSub
          emitNotification observer notification

timeoutWithScheduler :: (MonadIO m, MonadBaseControl IO m)
                     => Scheduler m
                     -> TimeInterval
                     -> Observable m v
                     -> Observable m v
timeoutWithScheduler scheduler =
  timeoutSelectorWithScheduler scheduler (const $ return True)

timeoutSelector :: (MonadIO m, MonadBaseControl IO m)
                => (v -> m Bool)
                -> TimeInterval
                -> Observable m v
                -> Observable m v
timeoutSelector pred =
  timeoutSelectorWithScheduler Scheduler.newThreadScheduler pred

timeout :: (MonadIO m, MonadBaseControl IO m)
                => TimeInterval
                -> Observable m v
                -> Observable m v
timeout =
  timeoutSelectorWithScheduler
      Scheduler.newThreadScheduler
      (const $ return True)

--------------------------------------------------------------------------------

timeoutAfterFirstSelectorWithScheduler
    :: (MonadIO m, MonadBaseControl IO m)
    => Scheduler m
    -> (v -> Bool)
    -> TimeInterval
    -> Observable m v
    -> Observable m v
timeoutAfterFirstSelectorWithScheduler scheduler predfn delay source =
    Observable $ \observer -> do
      allSub <- liftIO CS.create
      timeoutSub <- liftIO BS.empty
      innerObserverSub <- liftIO SAS.empty
      main allSub timeoutSub innerObserverSub observer
  where
    main allSub timeoutSub innerObserverSub observer = do
        subscription <- subscribe source $ Observer observerFn

        liftIO $ do
          SAS.set subscription innerObserverSub
          CS.append (toSubscription timeoutSub) allSub
          CS.append (toSubscription innerObserverSub) allSub

        return $ toSubscription allSub
      where
        resetTimeout = do
          subscription <- Scheduler.scheduleTimed scheduler delay $ do
            liftIO $ unsubscribe innerObserverSub
            let err = SomeException $ TimeoutException delay
            onError observer err
          liftIO $ BS.set subscription timeoutSub

        observerFn (OnNext v) = do
          when (predfn v) resetTimeout
          onNext observer v
        observerFn notification = do
          liftIO $ unsubscribe timeoutSub
          emitNotification observer notification


timeoutAfterFirstSelector :: (MonadIO m, MonadBaseControl IO m)
                          => (v -> Bool)
                          -> TimeInterval
                          -> Observable m v
                          -> Observable m v
timeoutAfterFirstSelector =
  timeoutAfterFirstSelectorWithScheduler Scheduler.newThreadScheduler

timeoutAfterFirstWithScheduler
    :: (MonadIO m, MonadBaseControl IO m)
    => Scheduler m
    -> TimeInterval
    -> Observable m v
    -> Observable m v
timeoutAfterFirstWithScheduler scheduler =
  timeoutAfterFirstSelectorWithScheduler scheduler (const True)

timeoutAfterFirst :: (MonadIO m, MonadBaseControl IO m)
                  => TimeInterval
                  -> Observable m v
                  -> Observable m v
timeoutAfterFirst =
  timeoutAfterFirstWithScheduler Scheduler.newThreadScheduler


completeOnTimeout :: (MonadIO m, MonadBaseControl IO m)
                  => TimeInterval
                  -> Observable m a
                  -> Observable m a
completeOnTimeout delay source =
  Observable $ \observer ->
    subscribe (timeout delay source)
      $ Observer
      $ \case
          OnNext v -> onNext observer v
          OnError err0@(SomeException err) -> do
            case cast err of
              Just (TimeoutException {}) ->
                onCompleted observer
              _ ->
                onError observer err0
          OnCompleted -> onCompleted observer
