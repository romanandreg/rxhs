{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
module Rx.Observable.Connectable (publish, multicast, connect) where

import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar
import Control.Monad (when, unless)
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)
import qualified Data.Sequence as Seq
import qualified Data.Foldable as Seq (toList)
import qualified Rx.Observer as Observer
import qualified Rx.Subscription.BooleanSubscription as BS
import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subject.PublishSubject as Subject
import Rx.Types


--------------------------------------------------------------------------------

-- | Returns a `ConnectableObservable`, which waits until its `connect`
-- method is called before it begins emitting items to those Observers
-- that have subscribed to it.
--
-- <<images/publish.png>>
--
publish :: (MonadIO m, MonadBaseControl IO m)
        => Observable m a
        -> m (ConnectableObservable m a)
publish source = do

  subject            <- Subject.create
  sourceSub          <- liftIO BS.empty
  connectedBufferVar <- liftIO $ TVar.newTVarIO Seq.empty
  isConnectedVar     <- liftIO $ TVar.newTVarIO False
  replayerVar        <- liftIO $ TVar.newTVarIO emptyReplayer

  sourceSub <- subscribe source
    $ Observer
    $ \case
        OnNext v -> do
          replayer <- liftIO . atomically $ TVar.readTVar replayerVar
          _replayerAddItem replayer $ OnNext v

          isConnected <- liftIO $ atomically
                           $ TVar.readTVar isConnectedVar
          if isConnected
             then onNext subject v
             else liftIO . atomically
                    $ TVar.modifyTVar connectedBufferVar (Seq.|> v)

        OnError err -> do
          replayer <- liftIO . atomically
                        $ TVar.readTVar replayerVar
          _replayerAddItem replayer $ OnError err
          onError subject err

        OnCompleted -> do
          replayer <- liftIO . atomically
                        $ TVar.readTVar replayerVar
          _replayerAddItem replayer OnCompleted
          onCompleted subject

  return $ ConnectableObservable isConnectedVar
                                 subject sourceSub
                                 connectedBufferVar
                                 replayerVar

--------------------------------------------------------------------------------

-- | Alias of `publish`
multicast :: (MonadIO m, MonadBaseControl IO m)
          => Observable m a
          -> m (ConnectableObservable m a)
multicast = publish

--------------------------------------------------------------------------------

-- | Allows a `ConnectableObservable` to emit values, see `publish`.
connect :: (MonadIO m, MonadBaseControl IO m)
        => ConnectableObservable m a
        -> m ()
connect (ConnectableObservable isConnectedVar subject
                               sourceSub connectedBufferVar _) = do

  connectedPending <- liftIO . atomically
                      $ TVar.readTVar connectedBufferVar
  liftIO . atomically
    $ TVar.writeTVar isConnectedVar True
  mapM_ (onNext subject) $ Seq.toList connectedPending


--------------------------------------------------------------------------------
