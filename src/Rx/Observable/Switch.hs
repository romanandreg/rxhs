{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Switch where

import Control.Monad (void)
import qualified Control.Concurrent.MVar.Lifted as MVar
import qualified Control.Concurrent.STM.TVar as TVar
import qualified Rx.Subscription.CompositeSubscription as CS
import qualified Rx.Subscription.BooleanSubscription as BS
import qualified Rx.Subscription.SingleAssignmentSubscription as SAS
import Rx.Observable.Core ()
import Rx.Types

switchOnNext :: (MonadIO m, MonadBaseControl IO m)
             => Observable m (Observable m v)
             -> Observable m v
switchOnNext sequences =
  Observable $ \observer -> do
    latestVar          <- liftIO $ TVar.newTVarIO 0
    onLatestVar        <- liftIO $ TVar.newTVarIO False
    stoppedVar         <- liftIO $ TVar.newTVarIO False
    parentSubscription <- liftIO SAS.empty
    switchSubscription <- liftIO BS.empty
    main parentSubscription switchSubscription
         latestVar onLatestVar stoppedVar
         observer
  where
    main parentSubscription switchSubscription
         latestVar onLatestVar stoppedVar
         observer = do
        cs <- liftIO CS.create
        sub <- subscribe sequences $ Observer switchObserver
        liftIO $ do
          SAS.set sub parentSubscription
          CS.append (toSubscription parentSubscription) cs
          CS.append (toSubscription switchSubscription) cs
          return $ toSubscription cs
      where
        getLatestIndex = liftIO . atomically $ TVar.readTVar latestVar
        getNextIndex = liftIO . atomically $ do
            TVar.modifyTVar latestVar succ
            TVar.writeTVar onLatestVar True
            TVar.readTVar latestVar

        switchObserver (OnNext source) = do
          sas <- liftIO SAS.empty
          index <- getNextIndex
          sub <- subscribe source $ Observer (innerObserver sas index)
          liftIO $ do
            SAS.set sub sas
            BS.set (toSubscription sas) switchSubscription

        switchObserver (OnError err) = do
          onError observer err
          liftIO $ unsubscribe parentSubscription

        switchObserver OnCompleted = do
          onLatest <- liftIO . atomically $ do
            TVar.writeTVar stoppedVar True
            TVar.readTVar onLatestVar
          unless onLatest $ do
            onCompleted observer
            liftIO $ unsubscribe parentSubscription

        innerObserver sub index (OnNext v) = do
          index' <- getLatestIndex
          if index == index'
             then onNext observer v
             else liftIO $ unsubscribe sub

        innerObserver sub index (OnError err) = do
          liftIO $ unsubscribe sub
          index' <- getLatestIndex
          when (index == index') $ do
            onError observer err
            liftIO $ unsubscribe parentSubscription

        innerObserver sub index OnCompleted = do
          index' <- getLatestIndex
          when (index == index') $ do
            isStopped <- liftIO . atomically $ do
              TVar.writeTVar onLatestVar False
              TVar.readTVar stoppedVar
            when isStopped $ do
              onCompleted observer
              liftIO $ unsubscribe parentSubscription
