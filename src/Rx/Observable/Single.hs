{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Single where

import qualified Control.Concurrent.MVar as MVar
import Control.Monad (unless, when)
import Control.Exception (toException, ErrorCall (..))
import Control.Monad.Trans (MonadIO(..))
import Control.Monad.Trans.Control (MonadBaseControl)
import Rx.Observable.Core ()
import Rx.Observer ()
import Rx.Types

singleWithCondition :: (MonadIO m, MonadBaseControl IO m)
                    => (a -> m Bool)
                    -> Observable m a
                    -> Observable m a
singleWithCondition predfn source =
    Observable $ \observer -> do
      singleValueVar <- liftIO $ MVar.newEmptyMVar
      subscribe source
        $ Observer
        $ \case
            (OnNext v) -> do
              isConditionMet <- predfn v
              when isConditionMet $ do
                wasAssigned <- liftIO $ MVar.tryPutMVar singleValueVar v
                unless wasAssigned $
                  onError observer
                          (toException
                           $ ErrorCall
                           $ "Expecting observable with single value, got multiple")
            OnCompleted -> do
              mValue <- liftIO $ MVar.tryTakeMVar singleValueVar
              case mValue of
                Just v -> onNext observer v
                Nothing ->
                  onError observer
                          (toException
                           $ ErrorCall
                           $ "Expecting observable with single value, got none")
            (OnError err) -> onError observer err

singleOrDefaultWithCondition :: (MonadIO m, MonadBaseControl IO m)
                             => (a -> m Bool)
                             -> a
                             -> Observable m a
                             -> Observable m a
singleOrDefaultWithCondition predfn defaultVal source =
    Observable $ \observer -> do
      singleValueVar <- liftIO $ MVar.newEmptyMVar
      subscribe source
        $ Observer
        $ \case
            (OnNext v) -> do
              isConditionMet <- predfn v
              when isConditionMet $ do
                wasAssigned <- liftIO $ MVar.tryPutMVar singleValueVar v
                unless wasAssigned $
                  onError observer
                          (toException
                           $ ErrorCall
                           $ "Expecting observable with single value, got multiple")
            OnCompleted -> do
              mValue <- liftIO $ MVar.tryTakeMVar singleValueVar
              onNext observer $ maybe defaultVal id mValue
            (OnError err) -> onError observer err


singleOrDefault :: (MonadIO m, MonadBaseControl IO m)
                => a
                -> Observable m a
                -> Observable m a
singleOrDefault = singleOrDefaultWithCondition (const $ return True)


single :: (MonadIO m, MonadBaseControl IO m)
       => Observable m a
       -> Observable m a
single = singleWithCondition (const $ return True)
