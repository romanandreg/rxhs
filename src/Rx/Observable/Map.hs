{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Map where

import Prelude hiding (concatMap, map)
import Rx.Observable.Core (decorateOnNext)
import Rx.Types

-- | Returns a new `Observable` that emits items resulting from
-- applying a monadic function that you supply to each item emitted by
-- the source `Observable`, where that function returns a List, and
-- then emitting the items inside that List.
--
-- <<images/concatMap.png>>
--
concatMapM :: (MonadIO m, MonadBaseControl IO m)
           => (v -> m [b]) -> Observable m v -> Observable m b
concatMapM fn source =
  Observable $ \observer ->
    subscribe source
      $ Observer
      $ decorateOnNext observer
      $ \v -> fn v >>= mapM_ (observer `onNext`)

-- | Returns an `Observable` that applies a specified monadic function
-- to each item emitted by the source `Observable` and emits the
-- results of these function applications.
--
-- <<images/map.png>>
--
mapM :: (MonadIO m, MonadBaseControl IO m)
      => (v -> m b) -> Observable m v -> Observable m b
mapM fn =
  concatMapM (\b -> fn b >>= return . return)

-- | Returns a new `Observable` that emits items resulting from
-- applying a pure function that you supply to each item emitted by
-- the source `Observable`, where that function returns a List, and
-- then emitting the items inside that List.
--
-- <<images/concatMap.png>>
--
concatMap :: (MonadIO m, MonadBaseControl IO m)
          => (v -> [b])
          -- ^ a function that, when applied to an item emitted by the
          -- source `Observable`, returns an `Observable`
          -> Observable m v
          -- ^ an `Observable` that emits the items from the source
          -- `Observable`, transformed by the specified function
          -> Observable m b
concatMap fn = concatMapM (return . fn)

-- | Returns an `Observable` that applies a specified pure function to
-- each item emitted by the source `Observable` and emits the results
-- of these function applications.
--
-- <<images/map.png>>
--
map :: (MonadIO m, MonadBaseControl IO m)
    => (v -> b)
    -- ^  a function to apply to each item emitted by the Observable
    -> Observable m v
    -- ^ an `Observable` that emits the items from the source
    -- `Observable`, transformed by the specified function
    -> Observable m b
map fn = concatMap (return . fn)
