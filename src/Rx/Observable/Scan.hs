{-# LANGUAGE FlexibleContexts #-}
module Rx.Observable.Scan where

import Control.Concurrent.STM (atomically)
import qualified Control.Concurrent.STM.TVar as TVar

import Rx.Observable.Core (decorateOnNext)
import Rx.Types

scan :: (MonadIO m, MonadBaseControl IO m)
     => (s -> a -> s) -> s
     -> Observable m a
     -> Observable m s
scan reducefn zero source = do
    Observable $ \observer -> do
      accVar <- liftIO $ TVar.newTVarIO zero
      subscribe source
        $ Observer
        $ decorateOnNext observer
        $ onNext' accVar observer
  where
    onNext' accVar observer val = do
      acc'  <- liftIO $ atomically $ do
        TVar.modifyTVar accVar (`reducefn` val)
        TVar.readTVar accVar
      onNext observer acc'

scanM :: (MonadIO m, MonadBaseControl IO m)
       => (s -> a -> m s) -> s
       -> Observable m a
       -> Observable m s
scanM reducefn zero source = do
    Observable $ \observer -> do
      accVar <- liftIO $ TVar.newTVarIO zero
      subscribe source
        $ Observer
        $ decorateOnNext observer
        $ onNext' accVar observer
  where
    onNext' accVar observer val = do
      acc  <- liftIO $ atomically $ TVar.readTVar accVar
      acc' <- reducefn acc val
      liftIO $ atomically $ TVar.writeTVar accVar acc'
      onNext observer acc'
